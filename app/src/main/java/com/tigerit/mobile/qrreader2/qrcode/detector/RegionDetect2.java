package com.tigerit.mobile.qrreader2.qrcode.detector;




        import android.graphics.Bitmap;
        import android.media.MediaScannerConnection;
        import android.net.Uri;
        import android.os.Environment;
        import android.util.Log;

        import com.tigerit.mobile.qrreader2.qrcode.IDecoderListener;

        import java.io.File;
        import java.io.FileOutputStream;
        import java.io.IOException;
        import java.io.OutputStream;
        import java.text.SimpleDateFormat;
        import java.util.ArrayList;
        import java.util.Date;

        import org.opencv.android.*;
        import org.opencv.core.Core;
        import org.opencv.core.CvType;
        import org.opencv.core.Mat;
        import org.opencv.core.MatOfPoint;
        import org.opencv.core.MatOfPoint2f;
        import org.opencv.core.Point;
        import org.opencv.core.Rect;
        import org.opencv.core.RotatedRect;
        import org.opencv.core.Scalar;
        import org.opencv.core.Size;
        import org.opencv.imgproc.Imgproc;
        import org.opencv.utils.Converters;

public class RegionDetect2 {
    public IDecoderListener activity = null;

    public ArrayList<Pair<Mat, Point[]>> Detect(Mat image){

        /**
         * Image preparation
         */

        Mat gray = new Mat(image.size(), CvType.CV_8UC1);
        //Imgproc.cvtColor(image, gray, Imgproc.COLOR_BGR2GRAY);
        image.copyTo(gray);


        Mat temp = new Mat(gray.size(), gray.depth());
        Imgproc.adaptiveBilateralFilter(gray, temp, new Size(3, 3), 2);
        temp.copyTo(gray);

        Imgproc.GaussianBlur(gray, gray, new Size(1, 1), 0);

        //Mat draw = new Mat(gray.size(), gray.depth());


        Mat kernel = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(3, 3));
        Imgproc.erode(gray, gray, kernel);

        //gray.copyTo(draw);
        //saveTrackingImage(draw, "Erode");

        //OpenCVUtils.ShowImage(gray);

        temp = gray.clone();
        Imgproc.GaussianBlur(gray, temp, new Size(0,0), 10);
        Core.addWeighted(gray, 2.5, temp, -2, 0, gray);

        //gray.copyTo(draw);
        //saveTrackingImage(draw, "add-weighted");



        Mat kernel2 = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(9, 9));
        Imgproc.erode(gray, gray, kernel2);
        //gray.copyTo(draw);

        //saveTrackingImage(draw, "erode-2");





        Mat thr = new Mat(gray.size(), gray.depth());
        gray.copyTo(thr);

        Imgproc.threshold(thr, thr, 220, 255, Imgproc.THRESH_OTSU);
        //saveTrackingImage(thr, "otsu");
        Imgproc.threshold(thr, thr, 0, 255, Imgproc.THRESH_BINARY_INV);
        //saveTrackingImage(thr, "binary-inv");



//        image.copyTo(draw);
//        ArrayList<MatOfPoint> tempCon = new ArrayList<MatOfPoint>();
//        Imgproc.findContours(thr.clone(), tempCon, new Mat(), Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE);
//        for (int i = 0 ; i < tempCon.size() ; i++){
//            Imgproc.drawContours(draw, tempCon, i, new Scalar(255, 255, 255));
//        }
//        saveTrackingImage(draw, "contours");

         ArrayList<MatOfPoint> contours = new ArrayList<MatOfPoint>();
        Imgproc.findContours(thr.clone(), contours, new Mat(), Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE);






        MatOfPoint2f approx = new MatOfPoint2f();

        ArrayList<MatOfPoint> squares = new ArrayList<MatOfPoint>();
        ArrayList<Integer> squareIndex = new ArrayList<Integer>();

        double area = 0.0;
        int index = -1;

        for (int i = 0; i < contours.size(); i++)
        {
            Imgproc.approxPolyDP(new MatOfPoint2f(contours.get(i).toArray()), approx,
                    Imgproc.arcLength(new MatOfPoint2f(contours.get(i).toArray()), true)*0.02, true);



            //if (approx.toArray().length == 4 )
            //{
//                Log.e("DETECT", "LENGTH 4 found");
//                double maxCosine = 0;
//
//                Point[] p = approx.toArray();
//                for( int j = 2; j < 5; j++ )
//                {
//                    double cosine = Math.abs(angle(p[j%4], p[j-2], p[j-1]));
//                    maxCosine = (maxCosine < cosine ? maxCosine : cosine);
//                }
//
//
//                if( maxCosine < 0.3 ){
                    double tArea = Imgproc.contourArea(approx);

                    Log.e("DETECT", "AREA : " + tArea);
                    if (tArea >= 1000){
                        RotatedRect box = Imgproc.minAreaRect(new MatOfPoint2f(contours.get(i).toArray()));
                        if (box != null) {
                            Point[] corners = new Point[4];
                            box.points(corners);
                            Point p1 = corners[0];
                            Point p2 = corners[1];
                            Point p3 = corners[2];
                            double l1 = Math.sqrt((p1.x - p2.x) * (p1.x - p2.x) + (p1.y - p2.y) * (p1.y - p2.y));
                            double l2 = Math.sqrt((p2.x - p3.x) * (p2.x - p3.x) + (p2.y - p3.y) * (p2.y - p3.y));
                            Log.e("DETECT", "REGION RATIO : " + (l1/l2));
                            if (l1 / l2 >= .75 && l1 / l2 <= 1.5) {
                                area = tArea;
                                index = i;
                            }
                        }
                        squares.add(new MatOfPoint(approx.toArray()));
                        squareIndex.add(i);
                    }

//                }
//            }

        }
        Log.e("DETECT", "REGIONS : " + squares.size());
        ArrayList<Pair<Mat, Point[]>> resultList = new ArrayList<Pair<Mat, Point[]>>();
        if (contours != null && contours.size() > 0 && squareIndex.size() > 0) {
            for (int i = 0; i < squareIndex.size(); i++) {

                int w = 0 ; int h = 0;

                RotatedRect box = Imgproc.minAreaRect(new MatOfPoint2f(contours.get(squareIndex.get(i)).toArray()));
                Rect rect = Imgproc.boundingRect(contours.get(squareIndex.get(i)));
                Mat ROI = image.submat(rect.y, rect.y + rect.height, rect.x, rect.x + rect.width);

                //w = box.boundingRect().width;
               // h = box.boundingRect().height;

                Mat qr_raw = Mat.zeros(300, 300, CvType.CV_8UC1);
                Point[] corners = new Point[4];


                box.points(corners);
                Log.e("DETECTED AREA", "AREA : " + area);


                ArrayList<Point> cornersList = new ArrayList<Point>();
                ArrayList<Point> transform = new ArrayList<Point>();

//                cornersList.add(corners[0]);
//                cornersList.add(corners[1]);
//                cornersList.add(corners[2]);
//                cornersList.add(corners[3]);

                cornersList.add(new Point(0, 0));
                cornersList.add(new Point(ROI.cols(), 0));
                cornersList.add(new Point(ROI.cols(), ROI.rows()));
                cornersList.add(new Point(0, ROI.rows()));

                Log.e("REGION", "Added Four Corners");

                /**
                 * Transforming to get the cropped image
                 */

                transform.add(new Point(20, 20));
                transform.add(new Point(280, 20));
                transform.add(new Point(280, 280));
                transform.add(new Point(20, 280));

                /**
                 * Now get the transformed qr
                 */
                Mat startM = Converters.vector_Point2f_to_Mat(cornersList);
                Mat endM = Converters.vector_Point2f_to_Mat(transform);


                Mat warp_matrix = Imgproc.getPerspectiveTransform(startM, endM);

                Imgproc.warpPerspective(ROI, qr_raw, warp_matrix, new Size(300, 300)); //prev image

                Mat temp2 = new Mat(qr_raw.size(), qr_raw.depth());


//                Imgproc.GaussianBlur(qr_raw, temp2, new Size(0, 0), 10);
//                Core.addWeighted(qr_raw, 1.5, temp2, -0.5, 0, qr_raw);
//
//                Imgproc.threshold(qr_raw, qr_raw, 0, 255 ,Imgproc.THRESH_OTSU);





                resultList.add(new Pair<Mat, Point[]>(qr_raw, corners));
                //saveTrackingImage(qr_raw, "qr-" + i + "-");

            }
        }


        return resultList;




    }

    private double angle(Point pt1, Point pt2, Point pt0)
    {
        double dx1 = pt1.x - pt0.x;
        double dy1 = pt1.y - pt0.y;
        double dx2 = pt2.x - pt0.x;
        double dy2 = pt2.y - pt0.y;
        return (dx1*dx2 + dy1*dy2)/Math.sqrt((dx1*dx1 + dy1*dy1)*(dx2*dx2 + dy2*dy2) + 1e-10);
    }

    private void saveTrackingImage(final Mat frame, final String tag){

        Thread t = new Thread(new Runnable(){
            public void run(){
                saveFile(frame, tag);
            }
        });
        t.start();
    }

    public void saveFile(Mat frame, String tag){
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String name = tag + "-tracking-" + timeStamp + ".png";
        Bitmap bmp = Bitmap.createBitmap(frame.width(), frame.height(), Bitmap.Config.ARGB_8888);
        org.opencv.android.Utils.matToBitmap(frame, bmp);
        File path = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        File file = new File(path, name);

        try {
            // Make sure the Pictures directory exists.
            path.mkdirs();

            // Very simple code to copy a picture from the application's
            // resource into the external file.  Note that this code does
            // no error checking, and assumes the picture is small (does not
            // try to copy it in chunks).  Note that if external storage is
            // not currently mounted this will silently fail.

            OutputStream os = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.PNG, 85, os);
            os.flush();
            os.close();


            // Tell the media scanner about the new file so that it is
            // immediately available to the user.
            MediaScannerConnection.scanFile(activity.getContext(),
                    new String[]{file.toString()}, null,
                    new MediaScannerConnection.OnScanCompletedListener() {
                        public void onScanCompleted(String path, Uri uri) {
                            Log.i("ExternalStorage", "Scanned " + path + ":");
                            Log.i("ExternalStorage", "-> uri=" + uri);
                        }
                    }
            );
        } catch (IOException e) {
            // Unable to create file, likely because external storage is
            // not currently mounted.
            Log.w("ExternalStorage", "Error writing " + file, e);
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

}


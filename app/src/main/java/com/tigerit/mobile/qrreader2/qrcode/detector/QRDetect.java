package com.tigerit.mobile.qrreader2.qrcode.detector;



import android.util.Log;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Vector;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;
import org.opencv.utils.Converters;

public class QRDetect {
	
	private final int QR_NORTH = 0;
	private final int QR_EAST = 1;
	private final int QR_SOUTH = 2;
	private final int QR_WEST = 3;
	
	//public Pair<List<Point>, Mat> Detect(Mat image){
    public Point[] Detect(Mat image, Mat draw){
		/**
		 * Gray
		 */
		Mat gray = new Mat(image.size(), CvType.CV_8UC1);
		image.copyTo(gray);
		
		/**
		 * Canny Edge
		 */
		Mat pcanny = new Mat(gray.size(), gray.depth());
		gray.copyTo(pcanny);
		Imgproc.Canny( pcanny, pcanny, 50, 150, 3, true );
		
		/**
		 * Canny to binary - clear
		 */
		Mat bin = new Mat(pcanny.size(), CvType.CV_8UC1);
		Imgproc.threshold(gray, bin, 0, 255, Imgproc.THRESH_OTSU);
		

		/**
		 * Find contours of certain size
		 */
		Mat contourMat = new Mat(bin.size(), CvType.CV_8UC1);
		bin.copyTo(contourMat);
		List<MatOfPoint> contours = findAndFilterConturs(contourMat, 8, 0.2 * image.cols() * image.rows());

		
		Log.e("DETECT", "Filtered Contours : " + contours.size());
		/**
		 * Sorting contours wrt to area
		 */
		if (contours.size() > 0){
			contours = Utils.SortMatOfPoint(contours);
		}else{
            return null;
        }

		
		/**
		 * Getting a pair list in which one contour is inside another - 3 finder pattern
		 */
		ArrayList<ArrayList<MatOfPoint>> concentricPairs = getConcentricContourPair(contours);
        Log.e("DETECT", "Concentrinc Contours : " + concentricPairs.size());

        if (concentricPairs.size() < 3)
            return null;

        if (concentricPairs.size() > 3)
		    concentricPairs = eliminatePairs(concentricPairs, 1, 6);
        Log.e("DETECT", "Concentrinc Contours after elimination : " + concentricPairs.size());

        if (concentricPairs.size() < 3)
            return null;
		
		/**
		 * Ordering the finder patterns - top-left, top-right, bottom-left
		 */
		concentricPairs = getFinderPattern(concentricPairs);

        //geting visualized image to show
        //Mat drawing = new Mat(image.size(), image.depth());
        //image.copyTo(drawing);

        //Imgproc.drawContours( draw, concentricPairs.get(0), 0, new Scalar(255, 255, 0), 1);
        //Imgproc.drawContours( draw, concentricPairs.get(1), 0, new Scalar(0, 255, 255), 1);
        //Imgproc.drawContours( draw, concentricPairs.get(2), 0, new Scalar(255, 0, 255), 1);
		
//		/**
//		 * Finding 4 corners
//		 */
		ArrayList<Point> corners = getFourCorners(concentricPairs);

        if (corners != null && corners.size() != 4)
            return null;

        return (Point[])corners.toArray();

		
//		/**
//		 * Transforming to get the cropped image
//		 */
//        ArrayList<Point> transform = new ArrayList<Point>();
//		transform.add(new Point(20, 20));
//		transform.add(new Point(120, 20));
//		transform.add(new Point(120, 120));
//		transform.add(new Point(20, 120));
//
//
//
//		/**
//		 * Now get the transformed qr
//		 */
//
//
//
//		Mat startM = Converters.vector_Point2f_to_Mat(corners);
//        Mat endM = Converters.vector_Point2f_to_Mat(transform);
//
//
//		Mat warp_matrix = Imgproc.getPerspectiveTransform(startM, endM);
//		Mat qr_raw = Mat.zeros(140, 140, CvType.CV_8UC1);
//        Imgproc.warpPerspective(gray, qr_raw, warp_matrix, new Size(140, 140));
//        Imgproc.copyMakeBorder( qr_raw, qr_raw, 10, 10, 10, 10, Imgproc.BORDER_CONSTANT, new Scalar(255,255,255) );
//		Imgproc.threshold(qr_raw, qr_raw, 0, 255, Imgproc.THRESH_OTSU);
//
//		//Imgproc.cvtColor (qr_raw, qr_raw, Imgproc.COLOR_RGB2GRAY);
//		//Imgproc.threshold(qr_raw, qr_raw, 100, 255, Imgproc.THRESH_OTSU);
//        qr_raw.convertTo(qr_raw, CvType.CV_8UC1);
//
//        //return new Pair<List<Point>, Mat>(corners, qr_raw);
//        return new Pair<Mat, Mat>(draw, qr_raw);

		
		
	}
	
	/**
	 * Retreive all contours filtered by size in pixels
	 * @param image
	 * @param minPix
	 * @param maxPix
	 * @return
	 */
	private List<MatOfPoint> findAndFilterConturs(Mat image, double minPix, double maxPix){
		List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
		Mat hierarchy = new Mat();
		Imgproc.findContours(image, contours, hierarchy, Imgproc.RETR_TREE, Imgproc.CHAIN_APPROX_SIMPLE);
		
		ArrayList<MatOfPoint> filteredList = new ArrayList<MatOfPoint>();
		for(MatOfPoint con : contours){
			double area = Imgproc.contourArea(con);
			if (area > minPix && area < maxPix) {
                filteredList.add(con);
            }
		}
		return filteredList;
	}
	
	/**
	 * Get concentric contours
	 * Return a list of pairs of which one is inside another
	 */
	private ArrayList<ArrayList<MatOfPoint>> getConcentricContourPair(List<MatOfPoint> contours){
		ArrayList<ArrayList<MatOfPoint>> listOfPair = new ArrayList<ArrayList<MatOfPoint>>();
		boolean [] bflag = new boolean[contours.size()];
		Arrays.fill(bflag, false);
		
		int szContours = contours.size();
		for(int i = 0; i < szContours - 1; i++){
			if(bflag[i]) continue;
			ArrayList<MatOfPoint> temp = new ArrayList<MatOfPoint>(); 
			temp.add(contours.get(i));
			for(int j = i + 1; j < szContours; j++){
				MatOfPoint2f c1 = new MatOfPoint2f(contours.get(j).toArray());
				MatOfPoint2f c2 = new MatOfPoint2f(contours.get(i).toArray());
				if(isConcentricContour(c1, c2)){
					temp.add(contours.get(j));
					bflag[j] = true;
				}
			}
			if(temp.size() > 1){
				listOfPair.add(temp);
			}
		}
		
		for(int i = 0; i < listOfPair.size(); i++){
			listOfPair.set(i,  (ArrayList<MatOfPoint>)Utils.SortMatOfPoint(listOfPair.get(i)));
		}
		return listOfPair;
	}
	
	/**
	 * If one contour is inside another
	 * @return
	 */
	private boolean isConcentricContour(MatOfPoint2f in, MatOfPoint2f out){
		/*
			The function determines whether the point is inside a contour, outside, or lies on 
			an edge (or coincides with a vertex). 
			It returns positive (inside), negative (outside), or zero (on an edge) value, 
			correspondingly. When measureDist=false, the return value is +1, -1, and 0, 
			respectively. 
			Otherwise, the return value is a signed distance between the point and the 
			nearest contour edge.
		*/

        //if (!isRectShape(new MatOfPoint(out.toArray())))
        //    return false;

		Point[] points = in.toArray();
		int sz = points.length;
		for(int i = 0; i < sz ; i++){
			if(Imgproc.pointPolygonTest(out, points[i], false) <= 0) 
				return false;
		}
		return true;
	}
	
	/**
	 * eliminate the pairs of concentric contours with some criteria
	 * @param listOfPairs
	 * @param minRatio
	 * @param maxRatio
	 * @return
	 */
	private ArrayList<ArrayList<MatOfPoint>> eliminatePairs(ArrayList<ArrayList<MatOfPoint>> listOfPairs, double minRatio, double maxRatio){
		Log.e("DETECT", "Eliminate Pair Called with " + listOfPairs.size());
        ArrayList<ArrayList<MatOfPoint>> filteredPairs = new ArrayList<ArrayList<MatOfPoint>>();
		
		int sz = listOfPairs.size();
		for (int m = 0 ; m < sz ; m++){


            ArrayList<MatOfPoint> pair = listOfPairs.get(m);
            if (!isRectShape(new MatOfPoint(pair.get(0))))
                continue;
			if (pair.size() < 3) continue;
			
			boolean flag = false;
			for(int i = 0; i < pair.size() - 1; i++){
				double area1 = Imgproc.contourArea(pair.get(i));
				double area2 = Imgproc.contourArea(pair.get(i + 1));
				//certain area ratio
				if(area1 / area2 < minRatio || area1 / area2 > maxRatio){
					flag = true;
					break;
				}
			}
			if (!flag)
				filteredPairs.add(pair);
		}
		
		if(filteredPairs.size() > 3){
			//further filtering
			filteredPairs = eliminatePairs(filteredPairs, minRatio, maxRatio * 0.9);
		}
		
		return filteredPairs;
	}
	
	/**
	 * Mass center of a contour
	 * @param vec
	 * @return
	 */
	private Point getContourCentre(MatOfPoint vec){
		double tempx = 0.0, tempy = 0.0;
		Point [] points = vec.toArray();
		int sz = points.length;
		for(int i=0; i < sz; i++){
			tempx += points[i].x;
			tempy += points[i].y;
		}
		return new Point(tempx / (double)sz, tempy / (double)sz);
	}
	
	/**
	 * Take the contour list and return 3 finder pattern in ordered manners
	 * TOP_LEFT, TOP_RIGHT, BOTTOM_LEFT
	 * @param patternCandidates
	 * @return
	 */
	private ArrayList<ArrayList<MatOfPoint>> getFinderPattern(ArrayList<ArrayList<MatOfPoint>> patternCandidates){
		ArrayList<MatOfPoint> pc1 = patternCandidates.get(0);
		ArrayList<MatOfPoint> pc2 = patternCandidates.get(1);
		ArrayList<MatOfPoint> pc3 = patternCandidates.get(2);
		
		ArrayList<MatOfPoint> c1 = pc1, c2 = pc2, c3 = pc3, topLeft, topRight, bottomLeft;
		
		Point pt1 = getContourCentre(pc1.get(pc1.size() - 1));
		Point pt2 = getContourCentre(pc2.get(pc2.size() - 1));
		Point pt3 = getContourCentre(pc3.get(pc3.size() - 1));
		
		double d12 = GeometricUtil.cv_distance(pt1, pt2);
		double d13 = GeometricUtil.cv_distance(pt1, pt3);
		double d23 = GeometricUtil.cv_distance(pt2, pt3);
		double x1, y1, x2, y2, x3, y3;
		double Max = Math.max(d12, Math.max(d13, d23));
		
		Point p1 = null, p2 = null, p3 = null;
		if(Max == d12){
			p1 = pt1; c1 = pc1;
			p2 = pt2; c2 = pc2;
			p3 = pt3; c3 = pc3;
		}else if(Max == d13){
			p1 = pt1; c1 = pc1;
			p2 = pt3; c2 = pc3;
			p3 = pt2; c3 = pc2;
		}else if(Max == d23){
			p1 = pt2; c1 = pc2;
			p2 = pt3; c2 = pc3;
			p3 = pt1; c3 = pc1;
		}
		x1 = p1.x; y1 = p1.y;
		x2 = p2.x; y2 = p2.y;
		x3 = p3.x; y3 = p3.y;
		
		if(x1 == x2){
			if(y1 > y2){
				if(x3 < x1){
					topLeft = c3;  topRight = c2; bottomLeft = c1;
				}else{
					topLeft = c3;  topRight = c1; bottomLeft = c2;
				}
			}else{
				if(x3 < x1){
					topLeft = c3; topRight = c1; bottomLeft = c2;
				}else{
					topLeft = c3; topRight = c2; bottomLeft = c1;
				}
			}
		}else{
			double newy = (y2 - y1) / (x2 - x1) * x3 + y1 - (y2 - y1) / (x2 - x1) * x1;
			if(x1 > x2){
				if(newy < y3){
					topLeft = c3; topRight = c2; bottomLeft = c1;
				}else{
					topLeft = c3; topRight = c1; bottomLeft = c2;
				}
			}else{
				if(newy < y3){
					topLeft = c3; topRight = c1; bottomLeft = c2;
				}else{
					topLeft = c3; topRight = c2; bottomLeft = c1;
				}
			}
		}
		
		ArrayList<ArrayList<MatOfPoint>> orderedList = new ArrayList<ArrayList<MatOfPoint>>();
		orderedList.add(topLeft);
		orderedList.add(topRight);
		orderedList.add(bottomLeft);
		return orderedList;
		
	}
	
	private ArrayList<Point> getFourCorners(ArrayList<ArrayList<MatOfPoint>> concentricPairs){
		ArrayList<MatOfPoint> topLeft = concentricPairs.get(0);
		ArrayList<MatOfPoint> topRight = concentricPairs.get(1);
		ArrayList<MatOfPoint> bottomRight = concentricPairs.get(2);
		
		Point mc_TopLeft = getContourCentre(topLeft.get(topLeft.size() - 1));
		Point mc_TopRight = getContourCentre(topRight.get(topRight.size() - 1));
		Point mc_BottomLeftPoint = getContourCentre(bottomRight.get(bottomRight.size() - 1));
		
		// Get the Perpendicular distance of the outlier from the longest side
		double dist = GeometricUtil.cv_lineEquation(mc_TopRight, mc_BottomLeftPoint, mc_TopLeft);	
        

        Pair<Double, Integer> p = GeometricUtil.cv_lineSlope(mc_TopRight, mc_BottomLeftPoint);		// Also calculate the slope of the longest side
        double slope = p.getFirst();
        int align = p.getSecond();
        
        int orientation = 0;
        if (align != 0){
	        if (slope < 0 && dist < 0 )		// Orientation - North
	        {
	            orientation = QR_NORTH;
	        }
	        else if (slope > 0 && dist < 0 )		// Orientation - East
	        {
	            orientation = QR_EAST;
	        }
	        else if (slope < 0 && dist > 0 )		// Orientation - South
	        {
	            orientation = QR_SOUTH;
	        }
	        else if (slope > 0 && dist > 0 )		// Orientation - West
	        {
	            orientation = QR_WEST;
	        }
        }
        
        System.out.println("Orientation : " + orientation);
        
        Vector<Point> tempL = GeometricUtil.cv_getVertices(topLeft, 0, slope);
        Vector<Point> tempM = GeometricUtil.cv_getVertices(topRight, 0, slope);
        Vector<Point> tempO = GeometricUtil.cv_getVertices(bottomRight, 0, slope);
        
//        Vector<Point> L  = tempL; //= GeometricUtil.cv_updateCornerOr(orientation, tempL);            // Re-arrange marker corners w.r.t orientation of the QR code
//        Vector<Point> M = tempM; //GeometricUtil.cv_updateCornerOr(orientation, tempM);            // Re-arrange marker corners w.r.t orientation of the QR code
//        Vector<Point> O = tempO; //GeometricUtil.cv_updateCornerOr(orientation, tempO);   
//        
        Vector<Point> L = GeometricUtil.cv_updateCornerOr(orientation, tempL);            // Re-arrange marker corners w.r.t orientation of the QR code
        Vector<Point> M = GeometricUtil.cv_updateCornerOr(orientation, tempM);            // Re-arrange marker corners w.r.t orientation of the QR code
        Vector<Point> O = GeometricUtil.cv_updateCornerOr(orientation, tempO);   
        
        Pair<Boolean, Point> pBP = GeometricUtil.getIntersectionPoint(M.get(1), M.get(2), O.get(3), O.get(2));
        Point N = pBP.getSecond();
        
        ArrayList<Point> src = new ArrayList<Point>();
        src.add(L.get(0));
        src.add(M.get(1));
        src.add(N);
        src.add(O.get(3));
        
        return src;
	}

    private boolean isRectShape(MatOfPoint contour){
        MatOfPoint2f contour2f = new MatOfPoint2f(contour.toArray());
        MatOfPoint2f approx = new MatOfPoint2f();
        //MatOfPoint2f curve, MatOfPoint2f approxCurve, double epsilon, boolean closed
        Imgproc.approxPolyDP(contour2f, approx, Imgproc.arcLength(contour2f, true) * 0.02, true);
// Skip small or non-convex objects
        MatOfPoint pp = new MatOfPoint(approx.toArray());
        if (!Imgproc.isContourConvex(pp))
            return false;

        int sz = approx.toArray().length;
        if (sz == 4)
            return true;
        return false;

//            int vtc = sz;
//        // Get the cosines of all corners
//            Vector<Double> cos = new Vector<Double>();
//            for (int j = 2; j < vtc+1; j++)
//                cos.push_back(angle(approx[j%vtc], approx[j-2], approx[j-1]));
//// Sort ascending the cosine values
//            std::sort(cos.begin(), cos.end());
//// Get the lowest and the highest cosine
//            double mincos = cos.front();
//            double maxcos = cos.back();
//// Use the degrees obtained above and the number of vertices
//// to determine the shape of the contour
//            if (vtc == 4 && mincos >= -0.1 && maxcos <= 0.3)
//                setLabel(dst, "RECT", contours[i]);
//            else if (vtc == 5 && mincos >= -0.34 && maxcos <= -0.27)
//                setLabel(dst, "PENTA", contours[i]);
//            else if (vtc == 6 && mincos >= -0.55 && maxcos <= -0.45)
//                setLabel(dst, "HEXA", contours[i]);
//        }
    }



}

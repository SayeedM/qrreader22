
package com.tigerit.mobile.qrreader2.qrcode.task;

import android.app.Activity;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.graphics.Rect;
import android.graphics.YuvImage;
import android.media.MediaPlayer;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import com.google.zxing.BarcodeFormat;
import com.tigerit.mobile.qrreader2.R;
import com.tigerit.mobile.qrreader2.qrcode.IDecoderListener;
import com.tigerit.mobile.qrreader2.qrcode.camera.CameraManager;
import com.tigerit.mobile.qrreader2.qrcode.detector.Pair;

import org.opencv.android.Utils;
import org.opencv.core.Mat;
import org.opencv.core.Point;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;


public final class DecoderActivityHandler extends Handler {

    private static final String TAG = DecoderActivityHandler.class.getSimpleName();

    private final IDecoderListener activity;
    //private final DecodeThread decodeThread;
    private final TrackingThread trackingThread;
    private final CameraManager cameraManager;
    private State state;

    private MediaPlayer sound;



    private enum State {
        PREVIEW, SUCCESS, DONE
    }

    public DecoderActivityHandler(IDecoderListener activity, Collection<BarcodeFormat> decodeFormats, String characterSet,
                           CameraManager cameraManager) {
        this.activity = activity;

        try {
            this.sound = new MediaPlayer();
            AssetFileDescriptor descriptor = activity.getContext().getAssets().openFd("shutter.wav");
            sound.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();
            sound.prepare();
            sound.setVolume(1f, 1f);
        }catch (Exception ex){
            ex.printStackTrace();
            sound = null;
            Log.e("SOUND", "Shutter Sound Null");
        }


        //decodeThread = new DecodeThread(activity, decodeFormats, characterSet, new ViewfinderResultPointCallback(activity.getViewfinder()));
        //decodeThread.start();
        trackingThread = new TrackingThread(activity);
        trackingThread.start();
        state = State.SUCCESS;


        // Start ourselves capturing previews and decoding.
        this.cameraManager = cameraManager;
        cameraManager.startPreview();
        restartPreviewAndDecode();
    }

    @Override
    public void handleMessage(Message message) {
        switch (message.what) {
            case R.id.auto_focus:
                // When one auto focus pass finishes, start another. This is the
                // closest thing to
                // continuous AF. It does seem to hunt a bit, but I'm not sure
                // what else to do.
//                if (state == State.PREVIEW)
//                    cameraManager.requestAutoFocus(this, R.id.auto_focus);

                break;
            case R.id.auto_focus_fail:
                // When one auto focus pass finishes, start another. This is the
                // closest thing to
                // continuous AF. It does seem to hunt a bit, but I'm not sure
                // what else to do.
//                if (state == State.PREVIEW)
//                    cameraManager.requestAutoFocus(this, R.id.auto_focus);
                activity.getViewfinder().setAutoFocusMsg("Autofocus fail");
                break;
            case R.id.auto_focus_clear:
                // When one auto focus pass finishes, start another. This is the
                // closest thing to
                // continuous AF. It does seem to hunt a bit, but I'm not sure
                // what else to do.
//                if (state == State.PREVIEW)
//                    cameraManager.requestAutoFocus(this, R.id.auto_focus);
                activity.getViewfinder().setAutoFocusMsg("");
                break;
            case R.id.restart_preview:
                Log.d(TAG, "Got restart preview message");
                restartPreviewAndDecode();
                break;
            case R.id.decode_succeeded:
                if (sound != null){
                    if (sound.isPlaying())
                        sound.stop();
                    sound.start();
                }
                Log.d(TAG, "Got decode succeeded message");
                state = State.SUCCESS;
                Bundle bundle = message.getData();

                activity.getViewfinder().setDecodeMsg("");

                final Pair<String, Bitmap> qr = (Pair<String, Bitmap>)message.obj;
                activity.handleDecode(qr.getFirst(), qr.getSecond());

                String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
                saveFile(qr.getSecond(), "success-" + timeStamp + ".png");


//                final Handler handler = new Handler();
//                handler.postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//
//                    }
//                }, 100);
                break;

            case R.id.track_success:
                Log.e(TAG, "handler track success");
                removeMessages(R.id.track_failed);

                activity.getViewfinder().setTrackMsg("");
                activity.getViewfinder().postInvalidate();
//
//                Pair<Mat, Point[]> p = (Pair<Mat, Point[]>)message.obj;
//                Point [] points = p.getSecond();
//
//                activity.getViewfinder().setTrackingData(points);
//                activity.getViewfinder().postInvalidate();
//
//                Mat sMat = p.getFirst();
//                Bitmap bmp2 = Bitmap.createBitmap(sMat.width(), sMat.height(), Bitmap.Config.ARGB_8888);
//                org.opencv.android.Utils.matToBitmap(sMat, bmp2);
//                final Bitmap bmp3 = bmp2;
//                Handler handler1 = new Handler();
//                handler1.postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        activity.handleTrack(bmp3);
//                    }
//                }, 1000);



                //(Point[])message.obj;
                //activity.getViewfinder().setTrackingData(points);
                //activity.getViewfinder().postInvalidate();
                //activity.getViewfinder().showStillMessage("Please hold still");


                //cameraManager.requestPreviewFrame(trackingThread.getHandler(), R.id.track_polygon);
//                Mat sMat = p.getFirst();
//                Bitmap bmp2 = Bitmap.createBitmap(sMat.width(), sMat.height(), Bitmap.Config.ARGB_8888);
//                org.opencv.android.Utils.matToBitmap(sMat, bmp2);
//                final Bitmap bmp3 = bmp2;
//                Handler handler1 = new Handler();
//                handler1.postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        activity.handleTrack(bmp3);
//                    }
//                }, 1000);



                //cameraManager.requestPreviewFrame(trackingThread.getHandler(), R.id.track_polygon);
//PREVIOUS CODES
//                final Pair<Pair<Mat, ArrayList<Point>>, Mat> pair = (Pair<Pair<Mat, ArrayList<Point>>, Mat>)message.obj;
//
//                Mat pMat = pair.getFirst().getFirst();
//                Bitmap bmp = Bitmap.createBitmap(pMat.width(), pMat.height(), Bitmap.Config.ARGB_8888);
//                org.opencv.android.Utils.matToBitmap(pMat, bmp);
//                activity.getViewfinder().drawResultBitmap(bmp);
//                activity.getViewfinder().setTrackingData(pair.getFirst().getSecond());
//                activity.getViewfinder().postInvalidate();
//
//                Mat sMat = pair.getSecond();
//                Bitmap bmp2 = Bitmap.createBitmap(sMat.width(), sMat.height(), Bitmap.Config.ARGB_8888);
//                org.opencv.android.Utils.matToBitmap(sMat, bmp2);
//                final Bitmap bmp3 = bmp2;
//                Handler handler1 = new Handler();
//                handler1.postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        activity.handleTrack(bmp3);
//                    }
//                }, 1000);




                break;
            case R.id.decode_failed:
                // We're decoding as fast as possible, so when one decode fails,
                // start another.
                activity.getViewfinder().setDecodeMsg("Decode Failure");
                state = State.PREVIEW;

                cameraManager.requestPreviewFrame(trackingThread.getHandler(), R.id.track_polygon);
                final ArrayList<Pair<Mat, Point[]>> dumpData = (ArrayList<Pair<Mat, Point[]>>)message.obj;
                Thread t = new Thread(new Runnable(){
                    public void run(){
                        for (int i = 0 ; i < dumpData.size() ; i++){
                            Pair<Mat, Point[]> region = dumpData.get(i);
                            Mat qr = region.getFirst();
                            Bitmap qrB = Bitmap.createBitmap(qr.width(), qr.height(), Bitmap.Config.ARGB_8888);
                            Utils.matToBitmap(qr, qrB);

                            Log.e(TAG, "QR SIZE :" + qrB.getWidth() + " " + qrB.getHeight());
                            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
                            saveFile(qrB, "failed-" + timeStamp + ".png");
                        }
                    }
                });
                t.start();
                //activity.handleFailure();

                break;

            case R.id.track_failed:
//                activity.getViewfinder().clearTrackingData();
//                activity.getViewfinder().postInvalidate();
//                activity.getViewfinder().showStillMessage("");
                activity.getViewfinder().setTrackMsg("Track Failure");
                activity.getViewfinder().postInvalidate();
                state = State.PREVIEW;
                cameraManager.requestPreviewFrame(trackingThread.getHandler(), R.id.track_polygon);
                break;
            case R.id.return_scan_result:
                Log.d(TAG, "Got return scan result message");
                if (activity instanceof Activity) {
                    ((Activity) activity).setResult(Activity.RESULT_OK, (Intent) message.obj);
                    ((Activity) activity).finish();
                } else {
                    Log.e(TAG, "Scan result message, activity is not Activity. Doing nothing.");
                }
                break;
            case R.id.preview_view:
                byte [] data = (byte[])message.obj;
                YuvImage image = new YuvImage(data, ImageFormat.NV21,
                        message.arg1, message.arg2, null);
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                image.compressToJpeg(new Rect(0, 0, message.arg1, message.arg2), 90, baos);
                Bitmap bmp = BitmapFactory.decodeByteArray(baos.toByteArray(), 0, baos.size());
                //activity.handleTrack(bmp);
                break;
            case R.id.dump_data:

                break;


        }
    }

    public void quitSynchronously() {
        state = State.DONE;
        cameraManager.stopPreview();
        Message quit = Message.obtain(trackingThread.getHandler(), R.id.quit);
        quit.sendToTarget();
        try {
            // Wait at most half a second; should be enough time, and onPause()
            // will timeout quickly
            trackingThread.join(500L);
            if (sound != null) {
                sound.stop();
                sound.release();
            }

        } catch (InterruptedException e) {
            // continue
        }

        // Be absolutely sure we don't send any queued up messages
        removeMessages(R.id.decode_succeeded);
        removeMessages(R.id.decode_failed);
        removeMessages(R.id.track_success);
        removeMessages(R.id.track_polygon);
        removeMessages(R.id.track_failed);
    }

    void restartPreviewAndDecode() {
        if (state == State.SUCCESS) {
            state = State.PREVIEW;
            cameraManager.requestPreviewFrame(trackingThread.getHandler(), R.id.track_polygon);
            //cameraManager.requestAutoFocus(this, R.id.auto_focus);
            activity.getViewfinder().drawViewfinder();
        }
    }

    /**
     * Autofocus counter
     */
    public int autoFocusCount = 0 ;

    public void saveFile(Bitmap bmp, String name){
        File path = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        File file = new File(path, name);

        try {
            // Make sure the Pictures directory exists.
            path.mkdirs();

            // Very simple code to copy a picture from the application's
            // resource into the external file.  Note that this code does
            // no error checking, and assumes the picture is small (does not
            // try to copy it in chunks).  Note that if external storage is
            // not currently mounted this will silently fail.

            OutputStream os = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.PNG, 85, os);
            os.flush();
            os.close();


            // Tell the media scanner about the new file so that it is
            // immediately available to the user.
            MediaScannerConnection.scanFile(activity.getContext(),
                    new String[]{file.toString()}, null,
                    new MediaScannerConnection.OnScanCompletedListener() {
                        public void onScanCompleted(String path, Uri uri) {
                            Log.i("ExternalStorage", "Scanned " + path + ":");
                            Log.i("ExternalStorage", "-> uri=" + uri);
                        }
                    }
            );
        } catch (IOException e) {
            // Unable to create file, likely because external storage is
            // not currently mounted.
            Log.w("ExternalStorage", "Error writing " + file, e);
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

}

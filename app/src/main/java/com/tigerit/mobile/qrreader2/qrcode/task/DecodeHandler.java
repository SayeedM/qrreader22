

package com.tigerit.mobile.qrreader2.qrcode.task;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.graphics.Rect;
import android.graphics.YuvImage;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;


import com.google.zxing.BinaryBitmap;
import com.google.zxing.DecodeHintType;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.ReaderException;
import com.google.zxing.Result;
import com.google.zxing.ResultPoint;
import com.google.zxing.common.HybridBinarizer;
import com.tigerit.mobile.qrreader2.R;
import com.tigerit.mobile.qrreader2.qrcode.IDecoderListener;
import com.tigerit.mobile.qrreader2.qrcode.PlanarYUVLuminanceSource;


import java.util.Map;



final class DecodeHandler extends Handler {

    private static final String TAG = DecodeHandler.class.getSimpleName();

    private final IDecoderListener activity;
    private final MultiFormatReader multiFormatReader;
    private boolean running = true;

    public DecodeHandler(IDecoderListener activity, Map<DecodeHintType, Object> hints) {
        multiFormatReader = new MultiFormatReader();
        multiFormatReader.setHints(hints);
        this.activity = activity;
    }

    @Override
    public void handleMessage(Message message) {
        if (!running) {
            return;
        }
        switch (message.what) {
        case R.id.decode:
            //*** tricky part
//            BlobDetection bd = new BlobDetection();
//            int h = message.arg2;
//            int w = message.arg1;
//            byte [] data = (byte[])message.obj;
//            if (data != null) {
//                Log.e("DECODE HANDLER", "Data not null");
//                YuvImage yuvimage = new YuvImage(data, ImageFormat.NV21, w, h, null);
//                ByteArrayOutputStream baos = new ByteArrayOutputStream();
//                yuvimage.compressToJpeg(new Rect(0, 0, w, h), 80, baos);
//                byte[] jdata = baos.toByteArray();
//                BitmapFactory.Options bitmapFatoryOptions = new BitmapFactory.Options();
//                bitmapFatoryOptions.inPreferredConfig = Bitmap.Config.RGB_565;
//                Bitmap bmp = BitmapFactory.decodeByteArray(jdata, 0, jdata.length, bitmapFatoryOptions);
//
//
//                FileOutputStream out = null;
//                try {
//                    out = new FileOutputStream(Environment.getExternalStorageDirectory().getPath() + "/1.png");
//                    bmp.compress(Bitmap.CompressFormat.PNG, 100, out); // bmp is your Bitmap instance
//                    // PNG is a lossless format, the compression factor (100) is ignored
//                } catch (Exception e) {
//                    e.printStackTrace();
//                } finally {
//                    try {
//                        if (out != null) {
//                            out.close();
//                        }
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//                }
//
//
//                //Reference: http://tech.thecoolblogs.com/2013/02/get-bitmap-image-from-yuv-in-android.html#ixzz3RylDO9T5
//
//                if(bmp != null) {
//                    Log.e("DECODE HANDLER", "Bitmap not null");
//                    FastBitmap fb = new FastBitmap(bmp);
//                    fb.toGrayscale();
//
//
//                    Grayscale g = new Grayscale(Grayscale.Algorithm.Average);
//                    g.applyInPlace(fb);
//
//                    //SusanCornersDetector susan = new SusanCornersDetector();
//                    //ArrayList<IntPoint> lst = susan.ProcessImage(fb);
//
////If you want to detect using Harris
//HarrisCornersDetector harris = new HarrisCornersDetector();
//ArrayList<IntPoint> lst = harris .ProcessImage(fb);
//
//                    //fb.toRGB();
//                    //for (IntPoint p : lst) {
//                    //    fb.setRGB(p, 255, 0, 0);
//                    //}
//                    //ArrayList<Blob> blobList = bd.ProcessImage(fb);
//                    Log.e("Blob Size ", lst.size() + "");
//                    activity.getViewfinder().drawBlobs(lst);
//                }
//            }

            //*** tricky part

            decode((byte[]) message.obj, message.arg1, message.arg2);
            break;
        case R.id.quit:
            running = false;
            Looper.myLooper().quit();
            break;
        }
    }

    /**
     * Decode the data within the viewfinder rectangle, and time how long it
     * took. For efficiency, reuse the same reader objects from one decode to
     * the next.
     * 
     * @param data
     *            The YUV preview frame.
     * @param width
     *            The width of the preview frame.
     * @param height
     *            The height of the preview frame.
     */
    private void decode(byte[] data, int width, int height) {
        long start = System.currentTimeMillis();
        Result rawResult = null;

        //***PORTRAIT MODE
        byte[] rotatedData = new byte[data.length];
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++)
                rotatedData[x * height + height - y - 1] = data[x + y * width];
        }
        data = rotatedData;
        int temp = width;
        width = height;
        height = temp;

        //***PORTRAIT MODE
        PlanarYUVLuminanceSource source = activity.getCameraManager().buildLuminanceSource(data, width, height);
        if (source != null) {
            BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));

            try {
                rawResult = multiFormatReader.decodeWithState(bitmap);
            } catch (ReaderException re) {
                // continue
            } finally {
                multiFormatReader.reset();
            }
        }

        Handler handler = activity.getHandler();
        if (rawResult != null) {
            // Don't log the barcode contents for security.
            long end = System.currentTimeMillis();
            Log.d(TAG, "Found barcode in " + (end - start) + " ms");
            Log.d(TAG, "FOUND barcode" + rawResult.getText());
            ResultPoint [] points = rawResult.getResultPoints();
            for(ResultPoint p : points){
                Log.e("POINT AFTER ACTIVITY ", p.getX() + "," + p.getY());
            }
            if (handler != null) {
                Message message = Message.obtain(handler, R.id.decode_succeeded, rawResult);
                Bundle bundle = new Bundle();
                bundle.putParcelable(DecodeThread.BARCODE_BITMAP, source.renderCroppedGreyscaleBitmap());
                message.setData(bundle);
                message.sendToTarget();
            }
        } else {
            if (handler != null) {
                Message message = Message.obtain(handler, R.id.decode_failed);
                message.sendToTarget();
            }
        }
    }

}

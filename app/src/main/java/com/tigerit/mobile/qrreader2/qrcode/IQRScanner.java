package com.tigerit.mobile.qrreader2.qrcode;

import android.graphics.Bitmap;

/**
 * Created by SayeedM on 3/7/2015.
 */
public interface IQRScanner {

    public void onQrScanned(String result, Bitmap image);
}



package com.tigerit.mobile.qrreader2.qrcode.camera;

import android.graphics.Point;
import android.hardware.Camera;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.tigerit.mobile.qrreader2.R;
import com.tigerit.mobile.qrreader2.qrcode.IDecoderListener;
import com.tigerit.mobile.qrreader2.qrcode.motion.data.GlobalData;
import com.tigerit.mobile.qrreader2.qrcode.motion.detection.RgbMotionDetection;
import com.tigerit.mobile.qrreader2.qrcode.motion.image.ImageProcessing;


final class PreviewCallback implements Camera.PreviewCallback {

    private static final String TAG = PreviewCallback.class.getSimpleName();

    private final CameraConfigurationManager configManager;
    private Handler previewHandler;
    private int previewMessage;

    private RgbMotionDetection motionDetect;

    private final IDecoderListener activity;


    public PreviewCallback(CameraConfigurationManager configManager, IDecoderListener activity) {
        this.configManager = configManager;
        this.activity = activity;
        motionDetect = new RgbMotionDetection();
    }

    void setHandler(Handler previewHandler, int previewMessage) {
        this.previewHandler = previewHandler;
        this.previewMessage = previewMessage;
    }




    @Override
    public void onPreviewFrame(byte[] data, Camera camera) {
        Point cameraResolution = configManager.getCameraResolution();

//        if (!GlobalData.isPhoneInMotion()) {
//            int[] rgb = ImageProcessing.decodeYUV420SPtoRGB(data, cameraResolution.x, cameraResolution.y);
//            boolean detected = motionDetect.detect(rgb, cameraResolution.x, cameraResolution.y);
//            if (detected) {
//                Log.e("PREVIEW", "Motion Detected");
//                Handler handler = activity.getHandler();
//                Message msg = Message.obtain(handler, R.id.track_failed);
//                msg.sendToTarget();
//                previewHandler = null;
//                return;
//            } else {
                Log.e("PREVIEW", "Motion not detected");
                Handler thePreviewHandler = previewHandler;
                if (thePreviewHandler != null) {
                    Message message = thePreviewHandler.obtainMessage(previewMessage, cameraResolution.x, cameraResolution.y, data);
                    message.sendToTarget();
                    previewHandler = null;
                } else {
                    Log.e(TAG, "Got preview callback, but no handler for it");
                }
            //}
//        }else{
//            Log.e("PREVIEW", "PHONE IN MOTION");
//        }
    }

    //TODO: manipulate later
    static public void decodeYUV420SP(int[] rgb, byte[] yuv420sp, int width, int height) {
        final int frameSize = width * height;

        for (int j = 0, yp = 0; j < height; j++) {
            int uvp = frameSize + (j >> 1) * width, u = 0, v = 0;
            for (int i = 0; i < width; i++, yp++) {
                int y = (0xff & ((int) yuv420sp[yp])) - 16;
                if (y < 0) y = 0;
                if ((i & 1) == 0) {
                    v = (0xff & yuv420sp[uvp++]) - 128;
                    u = (0xff & yuv420sp[uvp++]) - 128;
                }

                int y1192 = 1192 * y;
                int r = (y1192 + 1634 * v);
                int g = (y1192 - 833 * v - 400 * u);
                int b = (y1192 + 2066 * u);

                if (r < 0) r = 0; else if (r > 262143) r = 262143;
                if (g < 0) g = 0; else if (g > 262143) g = 262143;
                if (b < 0) b = 0; else if (b > 262143) b = 262143;

                rgb[yp] = 0xff000000 | ((r << 6) & 0xff0000) | ((g >> 2) & 0xff00) | ((b >> 10) & 0xff);
            }
        }
    }

}

package com.tigerit.mobile.qrreader2.qrcode.detector;

import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Rect;


public class DetectorResponse {
    public Mat qr;
    public Point[] corners;
    public Rect boundingBox;
}

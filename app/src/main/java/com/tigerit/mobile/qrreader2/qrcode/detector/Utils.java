package com.tigerit.mobile.qrreader2.qrcode.detector;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.imgproc.Imgproc;

public class Utils {
	
	public static List<MatOfPoint2f> sortMatOfPoint2f(List<MatOfPoint2f> lst){
		Collections.sort(lst, new Comparator<MatOfPoint2f>(){
			public int compare(MatOfPoint2f c1, MatOfPoint2f c2){
				double area1 = Imgproc.contourArea(c1);
				double area2 = Imgproc.contourArea(c2);
				if (Math.abs(area1) > Math.abs(area2))
					return 1;
				return 0;
			}
		});
		
		return lst;
	}
	
	public static List<MatOfPoint> SortMatOfPoint(List<MatOfPoint> lst){
		Collections.sort(lst, new Comparator<MatOfPoint>(){
			public int compare(MatOfPoint c1, MatOfPoint c2){
				double area1 = Imgproc.contourArea(c1);
				double area2 = Imgproc.contourArea(c2);
				if (Math.abs(area1) > Math.abs(area2))
					return 1;
				return 0;
			}
		});
		
		return lst;
	}

}

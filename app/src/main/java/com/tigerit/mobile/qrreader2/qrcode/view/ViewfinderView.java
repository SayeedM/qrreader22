

package com.tigerit.mobile.qrreader2.qrcode.view;

import com.google.zxing.ResultPoint;
import com.tigerit.mobile.qrreader2.R;
import com.tigerit.mobile.qrreader2.qrcode.camera.CameraManager;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import org.opencv.core.Point;

import java.util.ArrayList;
import java.util.List;


/**
 * This view is overlaid on top of the camera preview. It adds the viewfinder rectangle and partial
 * transparency outside it, as well as the laser scanner animation and result points.
 *
 * @author dswitkin@google.com (Daniel Switkin)
 */
public final class ViewfinderView extends View {

    private static final int[] SCANNER_ALPHA = {0, 64, 128, 192, 255, 192, 128, 64};
    private static final long ANIMATION_DELAY = 80L;
    private static final int CURRENT_POINT_OPACITY = 0xA0;
    private static final int MAX_RESULT_POINTS = 20;
    private static final int POINT_SIZE = 6;

    private CameraManager cameraManager;
    private final Paint paint;

    private final Paint cornerPaint;
    private final Paint borderPaint;
    private final Paint textPaint;


    private Bitmap resultBitmap;
    private final int maskColor;
    private final int resultColor;
    private final int laserColor;
    private final int resultPointColor;
    private int scannerAlpha;
    private List<ResultPoint> possibleResultPoints;
    private List<ResultPoint> lastPossibleResultPoints;
    private  Paint trackingPaint;

    private String trackMsg = "";
    private String autoFocusMsg = "" ;
    private String decodeMsg = "";



    // This constructor is used when the class is built from an XML resource.
    public ViewfinderView(Context context, AttributeSet attrs) {
        super(context, attrs);

        // Initialize these once for performance rather than calling them every time in onDraw().
        paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        Resources resources = getResources();
        maskColor = resources.getColor(R.color.viewfinder_mask);
        resultColor = resources.getColor(R.color.result_view);
        laserColor = resources.getColor(R.color.viewfinder_laser);
        resultPointColor = resources.getColor(R.color.possible_result_points);
        scannerAlpha = 0;
        possibleResultPoints = new ArrayList<ResultPoint>(5);
        lastPossibleResultPoints = null;

        borderPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        borderPaint.setStrokeWidth(4.0f);
        borderPaint.setColor(Color.BLACK);

        cornerPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        cornerPaint.setStrokeWidth(8.0f);
        cornerPaint.setColor(Color.WHITE);

        textPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        textPaint.setColor(Color.RED);
        textPaint.setTextSize(36);

        trackingPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        trackingPaint.setStrokeWidth(4.0f);
        trackingPaint.setColor(Color.YELLOW);
    }

    public void setCameraManager(CameraManager cameraManager) {
        this.cameraManager = cameraManager;
    }

    @Override
    public void onDraw(Canvas canvas) {
        if (cameraManager == null) {
            return; // not ready yet, early draw before done configuring
        }
        Rect frame = cameraManager.getFramingRect(canvas);
        if (frame == null) {
            return;
        }
        int width = canvas.getWidth();
        int height = canvas.getHeight();

        // Draw the exterior (i.e. outside the framing rect) darkened
        paint.setColor(resultBitmap != null ? resultColor : maskColor);


        canvas.drawRect(0, 0, width, frame.top, paint);
        canvas.drawRect(0, frame.top, frame.left, frame.bottom + 1, paint);
        canvas.drawRect(frame.right + 1, frame.top, width, frame.bottom + 1, paint);
        canvas.drawRect(0, frame.bottom + 1, width, height, paint);

        //top left corner
        canvas.drawLine(frame.left, frame.top, frame.left + 80, frame.top, cornerPaint);
        canvas.drawLine(frame.left, frame.top, frame.left, frame.top + 80, cornerPaint);

        //top right
        canvas.drawLine(frame.right, frame.top, frame.right - 80, frame.top, cornerPaint);
        canvas.drawLine(frame.right, frame.top, frame.right, frame.top + 80, cornerPaint);

        //bottom right
        canvas.drawLine(frame.right, frame.bottom, frame.right - 80, frame.bottom, cornerPaint);
        canvas.drawLine(frame.right, frame.bottom, frame.right, frame.bottom - 80, cornerPaint);

        //bottom left
        canvas.drawLine(frame.left, frame.bottom, frame.left + 80, frame.bottom, cornerPaint);
        canvas.drawLine(frame.left, frame.bottom, frame.left, frame.bottom - 80, cornerPaint);

        canvas.drawLine(frame.left + 80, frame.top, frame.right - 80, frame.top, borderPaint);
        canvas.drawLine(frame.left + 80, frame.bottom, frame.right - 80, frame.bottom, borderPaint);
        canvas.drawLine(frame.left, frame.top + 80, frame.left, frame.bottom - 80, borderPaint);
        canvas.drawLine(frame.right, frame.top + 80, frame.right, frame.bottom - 80, borderPaint);



        canvas.drawText("Please hold still", frame.width() / 2, frame.bottom - 60, textPaint);
        canvas.drawText(autoFocusMsg, frame.width() / 2, 30, textPaint);
        canvas.drawText(trackMsg, frame.width() / 2, 60, textPaint);
        canvas.drawText(decodeMsg, frame.width() / 2, 90, textPaint);

        float scaleX = frame.width() / (float) frame.width();
        float scaleY = frame.height() / (float) frame.height();


        if (trackingData != null) {
            synchronized (syncLock) {
                Log.e("ViewFinder", "Drawing tracking data");
                //for (Point point : trackingData){
                if (trackingData.length == 4) {


                    Point point1 = trackingData[0];
                    Point point2 = trackingData[1];
                    Point point3 = trackingData[2];
                    Point point4 = trackingData[3];

                    float p1_x = (float) (point1.x * scaleX);
                    float p1_y = (float) (point1.y * scaleY);
                    float p2_x = (float) (point2.x * scaleX);
                    float p2_y = (float) (point2.y * scaleY);
                    float p3_x = (float) (point3.x * scaleX);
                    float p3_y = (float) (point3.y * scaleY);
                    float p4_x = (float) (point4.x * scaleX);
                    float p4_y = (float) (point4.y * scaleY);
                    canvas.drawLine(p1_x, p1_y, p2_x, p2_y, trackingPaint);
                    canvas.drawLine(p2_x, p2_y, p3_x, p3_y, trackingPaint);
                    canvas.drawLine(p3_x, p3_y, p4_x, p4_y, trackingPaint);
                    canvas.drawLine(p4_x, p4_y, p1_x, p1_y, trackingPaint);

                    invalidate();
                }
            }
        }

        if (resultBitmap != null) {
            // Draw the opaque result bitmap over the scanning rectangle
            paint.setAlpha(CURRENT_POINT_OPACITY);
            canvas.drawBitmap(resultBitmap, null, frame, paint);
        } else {
            Rect previewFrame = cameraManager.getFramingRectInPreview();
            if (previewFrame == null) return;
            scaleX = frame.width() / (float) previewFrame.width();
            scaleY = frame.height() / (float) previewFrame.height();

            List<ResultPoint> currentPossible = possibleResultPoints;
            List<ResultPoint> currentLast = lastPossibleResultPoints;
            int frameLeft = frame.left;
            int frameTop = frame.top;
            if (currentPossible.isEmpty()) {
                lastPossibleResultPoints = null;
            } else {
                possibleResultPoints = new ArrayList<ResultPoint>(5);
                lastPossibleResultPoints = currentPossible;
                paint.setAlpha(CURRENT_POINT_OPACITY);
                paint.setColor(resultPointColor);
                synchronized (currentPossible) {
                    for (ResultPoint point : currentPossible) {
                        canvas.drawCircle(frameLeft + (int) (point.getX() * scaleX),
                                frameTop + (int) (point.getY() * scaleY),
                                POINT_SIZE, paint);
                    }
                }
            }
            if (currentLast != null) {
                paint.setAlpha(CURRENT_POINT_OPACITY / 2);
                paint.setColor(resultPointColor);
                synchronized (currentLast) {
                    float radius = POINT_SIZE / 2.0f;
                    for (ResultPoint point : currentLast) {
                        canvas.drawCircle(frameLeft + (int) (point.getX() * scaleX),
                                frameTop + (int) (point.getY() * scaleY),
                                radius, paint);
                    }
                }
            }

            // Request another update at the animation interval, but only repaint the laser line,
            // not the entire viewfinder mask.
            postInvalidateDelayed(ANIMATION_DELAY,
                    frame.left - POINT_SIZE,
                    frame.top - POINT_SIZE,
                    frame.right + POINT_SIZE,
                    frame.bottom + POINT_SIZE);
        }
    }

    public void drawViewfinder() {
        Bitmap resultBitmap = this.resultBitmap;
        this.resultBitmap = null;
        if (resultBitmap != null) {
            resultBitmap.recycle();
        }
        invalidate();
    }

    /**
     * Draw a bitmap with the result points highlighted instead of the live scanning display.
     *
     * @param barcode An image of the decoded barcode.
     */
    public void drawResultBitmap(Bitmap barcode) {
        resultBitmap = barcode;
        invalidate();
    }

    public void addPossibleResultPoint(ResultPoint point) {
        List<ResultPoint> points = possibleResultPoints;
        synchronized (points) {
            points.add(point);
            int size = points.size();
            if (size > MAX_RESULT_POINTS) {
                // trim it
                points.subList(0, size - MAX_RESULT_POINTS / 2).clear();
            }
        }
    }

    private Object syncLock = new Object();

    private Point[]  trackingData;
    public void setTrackingData(Point[] trackingData){
        if (trackingData == null || trackingData.length < 4)
            return;

        synchronized (syncLock) {
            this.trackingData = trackingData;
        }
        invalidate();
    }

    public void setAutoFocusMsg(String msg){
        this.autoFocusMsg = msg;
        invalidate();
    }
    public void setTrackMsg(String msg){
        this.trackMsg = msg;
        invalidate();
    }

    public void setDecodeMsg(String msg){
        this.decodeMsg = msg;
        invalidate();
    }

}

//import android.content.Context;
//import android.content.res.Resources;
//import android.graphics.Bitmap;
//import android.graphics.Canvas;
//import android.graphics.Color;
//import android.graphics.Paint;
//import android.graphics.PorterDuff;
//import android.graphics.Rect;
//import android.util.AttributeSet;
//import android.util.Log;
//import android.view.View;
//import android.widget.Button;
//
//
//import com.google.zxing.ResultPoint;
//import com.tigerit.mobile.qrreader2.R;
//import com.tigerit.mobile.qrreader2.qrcode.camera.CameraManager;
//import com.tigerit.mobile.qrreader2.qrcode.motion.data.GlobalData;
//
//
//import org.opencv.core.Point;
//
//import java.util.ArrayList;
//import java.util.Collections;
//import java.util.Comparator;
//import java.util.List;
//
//
//
////import com.google.zxing.ResultPoint;
////import com.jwetherell.quick_response_code.R;
////import com.jwetherell.quick_response_code.camera.CameraManager;
//
//public final class ViewfinderView extends View {
//
//
//    private CameraManager cameraManager;
//    //private final Paint paint;
//    private Bitmap resultBitmap;
//    private int cornerFrame;
//    private Paint trackingPaint;
//    private Paint bmpPaint;
//    private Paint textPaint;
//
//    private boolean alreadyDrawn = false;
//    private boolean hasChanged = false;
//
//    Button flashButton;
//    // This constructor is used when the class is built from an XML resource.
//    public ViewfinderView(Context context, AttributeSet attrs) {
//        super(context, attrs);
//
//        // Initialize these once for performance rather than calling them every
//        // time in onDraw().
//        bmpPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
//        bmpPaint.setAlpha(255);
//
//        textPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
//        textPaint.setColor(Color.RED);
//        textPaint.setTextSize(48);
//
//
//        Resources resources = getResources();
//        cornerFrame = resources.getColor(R.color.status_text);
//
//        trackingPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
//        trackingPaint.setColor(Color.YELLOW);
//        trackingPaint.setStrokeWidth(8.0f);
//
//
//    }
//
//    public void setCameraManager(CameraManager cameraManager) {
//        Log.e("View Finder", "Camera Manager Set");
//        this.cameraManager = cameraManager;
//    }
//
//
//
//    @Override
//    public void onDraw(Canvas canvas) {
//        Log.e("ViewFinder", "onDraw Entered");
//        Rect frame = cameraManager.getFramingRect();
//        if (frame == null) {
//            Log.e("ViewFinder", "frame null, returning");
//            return;
//        }
//        int width = canvas.getWidth();
//        int height = canvas.getHeight();
//
//        frame = new Rect(frame.left, frame.top, width - 20, height - 20);
//        Log.e("ViewFinder", "canvas h,w = " + (width + ", " + height));
//
//
////        int vW = frame.height() * 3 / 4;
////        int vH = frame.width() * 3 / 4;
////
////        int vX = (frame.height() - vW) / 2;
////        int vY = (frame.width() - vH) / 2;
////
////        canvas.drawRect(vX, vY, vW, vH, trackingPaint);
//
//
//
//
//
//
//        // Draw the exterior (i.e. outside the framing rect) darkened
////        paint.setColor(maskColor);
////        canvas.drawRect(0, 0, width, frame.top, paint);
////        canvas.drawRect(0, frame.top, frame.left, frame.bottom + 1, paint);
////        canvas.drawRect(frame.right + 1, frame.top, width, frame.bottom + 1, paint);
////        canvas.drawRect(0, frame.bottom + 1, width, height, paint);
//
//        Rect previewFrame = cameraManager.getFramingRectInPreview();
//        float scaleX = frame.width() / (float) previewFrame.width();
//        float scaleY = frame.height() / (float) previewFrame.height();
//
//        if (resultBitmap != null) {
//            Log.e("ViewFinder", "drawing previous bitmap");
//
//            canvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
//
//            canvas.drawBitmap(resultBitmap, null, frame, bmpPaint);
//
////            if (trackingData != null) {
////                synchronized (trackingData) {
////                    Log.e("ViewFinder", "Drawing tracking data");
////                    //for (Point point : trackingData){
////                    if (trackingData.size() == 4) {
////
////
////                        Point point1 = trackingData.get(0);
////                        Point point2 = trackingData.get(1);
////                        Point point3 = trackingData.get(2);
////                        Point point4 = trackingData.get(3);
////
////                        float p1_x = (float) (point1.x * scaleX);
////                        float p1_y = (float) (point1.y * scaleY);
////                        float p2_x = (float) (point2.x * scaleX);
////                        float p2_y = (float) (point2.y * scaleY);
////                        float p3_x = (float) (point3.x * scaleX);
////                        float p3_y = (float) (point3.y * scaleY);
////                        float p4_x = (float) (point4.x * scaleX);
////                        float p4_y = (float) (point4.y * scaleY);
////                        canvas.drawLine(p1_x, p1_y, p2_x, p2_y, trackingPaint);
////                        canvas.drawLine(p2_x, p2_y, p3_x, p3_y, trackingPaint);
////                        canvas.drawLine(p3_x, p3_y, p4_x, p4_y, trackingPaint);
////                        canvas.drawLine(p4_x, p4_y, p1_x, p1_y, trackingPaint);
////
////                        invalidate();
////                    }
////                }
//
//
//
//            }
//            // Draw a two pixel solid black border inside the framing rect
//            //paint.setColor(frameColor);
//            Paint p = new Paint();
//            p.setColor(cornerFrame);
//            p.setStrokeWidth(4.0f);
//
//            //top left corner
//            canvas.drawLine(frame.left + 20, frame.top + 20, frame.left + 40, frame.top + 20, p);
//            canvas.drawLine(frame.left + 20, frame.top + 20, frame.left + 20, frame.top + 40, p);
//
//            //top right
//            canvas.drawLine(frame.width(), frame.top + 20, frame.width() - 20, frame.top + 20, p);
//            canvas.drawLine(frame.width(), frame.top + 20, frame.width(), frame.top + 40, p);
//
//            //bottom right
//            canvas.drawLine(frame.width(), frame.height(), frame.width() - 20, frame.height(), p);
//            canvas.drawLine(frame.width(), frame.height(), frame.width(), frame.height() - 20, p);
//
//            //bottom left
//            canvas.drawLine(frame.left + 20, frame.height(), frame.left + 40, frame.height(), p);
//            canvas.drawLine(frame.left + 20, frame.height(), frame.left + 20, frame.height() - 20, p);
//
//
//            canvas.drawText(this.msg, frame.width() / 2, frame.height() - 60, textPaint);
//
//
//
//        if (trackingData != null) {
//            synchronized (trackingData) {
//                Log.e("ViewFinder", "Drawing tracking data");
//                //for (Point point : trackingData){
//                if (trackingData.size() == 4) {
//
//
//                    Point point1 = trackingData.get(0);
//                    Point point2 = trackingData.get(1);
//                    Point point3 = trackingData.get(2);
//                    Point point4 = trackingData.get(3);
//
//                    float p1_x = (float) (point1.x * scaleX);
//                    float p1_y = (float) (point1.y * scaleY);
//                    float p2_x = (float) (point2.x * scaleX);
//                    float p2_y = (float) (point2.y * scaleY);
//                    float p3_x = (float) (point3.x * scaleX);
//                    float p3_y = (float) (point3.y * scaleY);
//                    float p4_x = (float) (point4.x * scaleX);
//                    float p4_y = (float) (point4.y * scaleY);
//                    canvas.drawLine(p1_x, p1_y, p2_x, p2_y, trackingPaint);
//                    canvas.drawLine(p2_x, p2_y, p3_x, p3_y, trackingPaint);
//                    canvas.drawLine(p3_x, p3_y, p4_x, p4_y, trackingPaint);
//                    canvas.drawLine(p4_x, p4_y, p1_x, p1_y, trackingPaint);
//                    alreadyDrawn = true;
//                    invalidate();
//                }else{
//                    //Log.e("VIEW", "NOT DRAWING, ALREADY DRAWN");
//                    //alreadyDrawn = false;
//                }
//            }
//        }
//
//    }
//
//    public void drawViewfinder() {
//        Bitmap resultBitmap = this.resultBitmap;
//        this.resultBitmap = null;
//        if (resultBitmap != null) {
//            resultBitmap.recycle();
//        }
//        invalidate();
//    }
//
//    /**
//     * Draw a bitmap with the result points highlighted instead of the live
//     * scanning display.
//     *
//     * @param barcode
//     *            An image of the decoded barcode.
//     */
//    public void drawResultBitmap(Bitmap barcode) {
//        resultBitmap = barcode;
//        invalidate();
//    }
//
////    public void addPossibleResultPoint(ResultPoint point) {
////        List<ResultPoint> points = possibleResultPoints;
////        synchronized (point) {
////            points.add(point);
////            int size = points.size();
////            if (size > MAX_RESULT_POINTS) {
////                // trim it
////                points.subList(0, size - MAX_RESULT_POINTS / 2).clear();
////            }
////        }
////    }
////
////    public void clear(){
////
////    }
//
//
//    @Override
//    protected void onVisibilityChanged(View changedView, int visibility) {
//        super.onVisibilityChanged(changedView, visibility);
//        if (visibility != View.VISIBLE) {
//
//            if (trackingData != null){
//                trackingData.clear();
//            }
//        }
//    }
//
//
//    //ArrayList<Point> trackingData = new ArrayList<Point>();
//
//    ArrayList<Point> trackingData = new ArrayList<Point>();
//    private Object syncLock = new Object();
//    public void setTrackingData(Point [] trackingData){
//        if (trackingData == null || trackingData.length < 4)
//            return;
//
//
//        synchronized (syncLock) {
//            this.trackingPaint.setColor(Color.YELLOW);
//            //this.trackingData = new ArrayList<Point>();
//            if (this.trackingData != null && this.trackingData.size() == 4) {
//                boolean shouldChange = false;
//                for (int i = 0 ; i < 4 ; i++){
//                    Point p1 = this.trackingData.get(i);
//                    Point p2 = trackingData[i];
//
//                    double dist = Math.sqrt((p1.x - p2.x) * (p1.x - p2.x) + (p1.y - p2.y)  * (p1.y - p2.y));
//
//                    if (dist > 30){
//                        Log.e("VIEW", "threshold crossed");
//                        shouldChange = true;
//                        hasChanged = true;
//                        break;
//                    }
//
//                }
//                if (shouldChange) {
//                    for (Point p : trackingData)
//                        this.trackingData.add(p);
//                }else{
//                    hasChanged = false;
//                }
//            }else{
//                for (Point p : trackingData)
//                    this.trackingData.add(p);
//                hasChanged = true;
//            }
//        }
//        invalidate();
//    }
//
//    public void clearTrackingData(){
//        Log.e("VIEW", "Tracking data cleared");
//        synchronized (trackingData) {
//            this.trackingData.clear();
//        }
//        invalidate();
//    }
//
//    private String msg = "";
//    public void showStillMessage(String str){
//        this.msg = str;
//        invalidate();
//    }
//
//    public void wrongTrackingData(){
//        this.trackingPaint.setColor(Color.RED);
//        invalidate();
//    }
//
//
//
//
//
//}

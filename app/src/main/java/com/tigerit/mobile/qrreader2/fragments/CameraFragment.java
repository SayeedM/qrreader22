package com.tigerit.mobile.qrreader2.fragments;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;


import com.google.zxing.BarcodeFormat;
import com.google.zxing.Result;
import com.google.zxing.ResultPoint;
import com.tigerit.mobile.qrreader2.R;
import com.tigerit.mobile.qrreader2.qrcode.DecoderActivity;
import com.tigerit.mobile.qrreader2.qrcode.IDecoderListener;

import com.tigerit.mobile.qrreader2.qrcode.IQRScanner;
import com.tigerit.mobile.qrreader2.qrcode.camera.CameraManager;
import com.tigerit.mobile.qrreader2.qrcode.task.DecoderActivityHandler;
import com.tigerit.mobile.qrreader2.qrcode.view.ViewfinderView;


public class CameraFragment extends Fragment implements IQRScanner {

    private final String TAG = CameraFragment.class.getSimpleName();
    public static final int QR_DECODE = 1;

    private TextView txtResult;
    private ImageView imgResult;
    private CameraManager cameraManager;
    private ViewfinderView viewfinderView;


    private DecoderActivityHandler handler = null;



    //private QRScanner scanner;

    private Button toggleScanner;

    private SurfaceView surfaceView;

    private Button flashButton;
    boolean flashOn = false;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.camera_fragment, container, false);

        txtResult = (TextView)view.findViewById(R.id.txtResult);
        imgResult = (ImageView)view.findViewById(R.id.barcodeImage);

//        getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
//        this.surfaceView = (SurfaceView) view.findViewById(R.id.preview_view);
//        scanner = new QRScanner(this, surfaceView);
//
//        flashButton = (Button)view.findViewById(R.id.flash);
        return view;
    }

    @Override
    public void onQrScanned(String result, Bitmap image){

        txtResult.setText(result);
        imgResult.setImageBitmap(image);
    }

    @Override
    public void onResume(){
        super.onResume();
//        Log.v(TAG, "onResume()");
//
//        if (cameraManager == null)
//            cameraManager = new CameraManager(getActivity().getApplication(), this);
//
//
//        if (viewfinderView == null) {
//            viewfinderView = (ViewfinderView) getView().findViewById(R.id.viewfinder_view);
//            viewfinderView.setCameraManager(cameraManager);
//        }
        if (toggleScanner == null) {
            toggleScanner = (Button) getView().findViewById(R.id.toggleScanner);
            toggleScanner.setText("Scan");
            toggleScanner.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    if (scanner != null) {
//                        ((FrameLayout)getView().findViewById(R.id.container)).setVisibility(View.VISIBLE);
//                        surfaceView.setVisibility(View.VISIBLE);
//                        viewfinderView.setVisibility(View.VISIBLE);
//
//                        txtResult.setText("Decode Failed");
//                        scanner.showScanner();
//                    }
                    Intent ii = new Intent(getActivity(), DecoderActivity.class);
                    getActivity().startActivityForResult(ii, CameraFragment.QR_DECODE);
                }
            });
        }
//
//        if (flashButton != null){
//            flashButton = (Button)getView().findViewById(R.id.flash);
//            flashButton.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    cameraManager.toggleFlash();
//                }
//            });
//        }



    }

    @Override
    public void onPause() {
        super.onPause();
//        Log.v(TAG, "onPause()");
//        if (handler != null) {
//            handler.quitSynchronously();
//            handler = null;
//            System.gc();
//        }
//        cameraManager.closeDriver();
        //scanner.hideScanner();
    }

//    @Override
//    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        if (keyCode == KeyEvent.KEYCODE_FOCUS || keyCode == KeyEvent.KEYCODE_CAMERA) {
//            // Handle these events so they don't launch the Camera app
//            return true;
//        }
//        return super.onKeyDown(keyCode, event);
//    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.v(TAG, "onDestroy()");
    }

    protected void showScanner() {
        viewfinderView.setVisibility(View.VISIBLE);
    }



//    @Override
//    public void onScannerInitComplete(){
//        Log.e(TAG, "init complete");
//
//        if (handler != null){
//            handler.quitSynchronously();
//            handler = null;
//            System.gc();
//        }
//        handler = new DecoderActivityHandler(this, null, null, this.getCameraManager());
//    }

//    @Override
//    public ViewfinderView getViewfinder() {
//        return viewfinderView;
//    }
//
//    @Override
//    public android.os.Handler getHandler() {
//        return handler;
//    }
//
//    @Override
//    public CameraManager getCameraManager() {
//        return cameraManager;
//    }
//
//    @Override
//    public void handleDecode(String rawResult, Bitmap barcode) {
//        Log.e("HANDLE DECODE", "HANDLE DECODE");
//        //drawResultPoints(barcode, rawResult);
//        txtResult.setText(rawResult);
//        imgResult.setImageBitmap(barcode);
//        scanner.hideScanner();
//        (getView().findViewById(R.id.container)).setVisibility(View.GONE);
//    }
//
//    @Override
//    public void handleDecode(String text) {
//        txtResult.setText(text);
//
//        scanner.hideScanner();
//        (getView().findViewById(R.id.container)).setVisibility(View.GONE);
//    }
//
////    @Override
////    public void handleFailure(){
////        txtResult.setText("Decode Failed !!");
////    }
//
//    @Override
//    public SurfaceView getSurfaceView(){
//        return this.surfaceView;
//    }
//
//    @Override
//    public Context getContext(){
//        return getActivity();
//    }
//
//    protected void drawResultPoints(Bitmap barcode, Result rawResult) {
////        ResultPoint[] points = rawResult.getResultPoints();
////        if (points != null && points.length > 0) {
////            Canvas canvas = new Canvas(barcode);
////            Paint paint = new Paint();
////            paint.setColor(getResources().getColor(R.color.result_image_border));
////            paint.setStrokeWidth(3.0f);
////            paint.setStyle(Paint.Style.STROKE);
////            Rect border = new Rect(2, 2, barcode.getWidth() - 2, barcode.getHeight() - 2);
////            canvas.drawRect(border, paint);
////
////            paint.setColor(getResources().getColor(R.color.result_points));
////            if (points.length == 2) {
////                paint.setStrokeWidth(4.0f);
////                drawLine(canvas, paint, points[0], points[1]);
////            } else if (points.length == 4 && (rawResult.getBarcodeFormat() == BarcodeFormat.UPC_A || rawResult.getBarcodeFormat() == BarcodeFormat.EAN_13)) {
////                // Hacky special case -- draw two lines, for the barcode and
////                // metadata
////                drawLine(canvas, paint, points[0], points[1]);
////                drawLine(canvas, paint, points[2], points[3]);
////            } else {
////                paint.setStrokeWidth(10.0f);
////                for (ResultPoint point : points) {
////                    canvas.drawPoint(point.getX(), point.getY(), paint);
////                }
////            }
////        }
//    }

    protected static void drawLine(Canvas canvas, Paint paint, ResultPoint a, ResultPoint b) {
        canvas.drawLine(a.getX(), a.getY(), b.getX(), b.getY(), paint);
    }

//    @Override
//    public void overlay(Bitmap bmp){
//        surfaceView.setVisibility(View.INVISIBLE);
//        viewfinderView.setVisibility(View.INVISIBLE);
//
//    }
//    @Override
//    public void handleTrack(Bitmap bmp){
//        Log.e("HANDLE DECODE", "HANDLE DECODE");
//        imgResult.setImageBitmap(bmp);
//        scanner.hideScanner();
//        (getView().findViewById(R.id.container)).setVisibility(View.GONE);
//    }


}

//package com.tigerit.mobile.qrreader2.qrcode;
//
//
//import android.os.Message;
//import android.util.Log;
//import android.view.SurfaceHolder;
//import android.view.SurfaceView;
//import android.view.View;
//
//
//import java.io.IOException;
//
//public class QRScanner  implements SurfaceHolder.Callback {
//    private final String TAG = QRScanner.class.getSimpleName();
//
//    private SurfaceView surfaceView;
//    private IDecoderListener listener;
//
//
//    private boolean hasSurface = false;
//
//    public QRScanner(IDecoderListener listener, SurfaceView surfaceView){
//        this.surfaceView = surfaceView;
//        this.listener = listener;
//
//    }
//
//    public void showScanner(){
//        initScanner();
//        listener.getViewfinder().setVisibility(View.VISIBLE);
//    }
//
//    public void hideScanner(){
//
//        listener.getViewfinder().setVisibility(View.GONE);
//        deinitScanner();
//        //listener.getCameraManager().flashOff();
//    }
//
//    private void initScanner(){
//        SurfaceHolder surfaceHolder = surfaceView.getHolder();
//        if (hasSurface) {
//            // The activity was paused but not stopped, so the surface still
//            // exists. Therefore
//            // surfaceCreated() won't be called, so init the camera here.
//            initCamera(surfaceHolder);
//            //listener.onScannerInitComplete();
//
//        } else {
//            // Install the callback and wait for surfaceCreated() to init the
//            // camera.
//            surfaceHolder.addCallback(this);
//            surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
//        }
//    }
//
//
//    private void deinitScanner(){
//        if (!hasSurface) {
//            SurfaceHolder surfaceHolder = surfaceView.getHolder();
//            surfaceHolder.removeCallback(this);
//        }
//        listener.getCameraManager().closeDriver();
//        destroyImageProcessingThread();
//    }
//
//    /**
//     * Surface Holder Callbacks
//     *
//     */
//    @Override
//    public void surfaceCreated(SurfaceHolder holder) {
//        if (holder == null)
//            Log.e(TAG, "*** WARNING *** surfaceCreated() gave us a null surface!");
//        if (!hasSurface) {
//            hasSurface = true;
//            initCamera(holder);
//            listener.onScannerInitComplete();
//        }
//    }
//
//    @Override
//    public void surfaceDestroyed(SurfaceHolder holder) {
//        hasSurface = false;
//    }
//
//    @Override
//    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
//        // Ignore
//    }
//
//
//
//    protected void initCamera(SurfaceHolder surfaceHolder) {
//        try {
//            listener.getCameraManager().openDriver(surfaceHolder);
//
//
//        } catch (IOException ioe) {
//            Log.w(TAG, ioe);
//        } catch (RuntimeException e) {
//            // Barcode Scanner has seen crashes in the wild of this variety:
//            // java.?lang.?RuntimeException: Fail to connect to camera service
//            Log.w(TAG, "Unexpected error initializing camera", e);
//        }
//    }
//
//    public void destroyImageProcessingThread(){
////        if (processingThread != null){
////            Message quit = Message.obtain(processingThread.getHandler(), R.id.quit);
////            quit.sendToTarget();
////            try {
////                // Wait at most half a second; should be enough time, and onPause()
////                // will timeout quickly
////                processingThread.join(500L);
////            } catch (InterruptedException e) {
////                // continue
////            }
////        }
//    }
//}

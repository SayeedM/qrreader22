

package com.tigerit.mobile.qrreader2.qrcode.camera;

import android.content.Context;
import android.graphics.ImageFormat;
import android.graphics.Point;
import android.graphics.Rect;
import android.hardware.Camera;
import android.os.Build;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * A class which deals with reading, parsing, and setting the camera parameters
 * which are used to configure the camera hardware.
 */
public final class CameraConfigurationManager {

    private static final String TAG = "CameraConfiguration";
    private static final int MIN_PREVIEW_PIXELS = 320 * 240; // small screen
    private static final int MAX_PREVIEW_PIXELS = 800 * 480; // large/HD screen

    private final Context context;
    private Point screenResolution;
    private Point cameraResolution;

    public CameraConfigurationManager(Context context) {
        this.context = context;
    }

    /**
     * Reads, one time, values from the camera that are needed by the app.
     */
    void initFromCameraParameters(Camera camera) {
        Camera.Parameters parameters = camera.getParameters();
        WindowManager manager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = manager.getDefaultDisplay();
        int width = display.getWidth();
        int height = display.getHeight();
        // We're landscape-only, and have apparently seen issues with display
        // thinking it's portrait
        // when waking from sleep. If it's not landscape, assume it's mistaken
        // and reverse them:
        if (width < height) {
            Log.i(TAG, "Display reports portrait orientation; assuming this is incorrect");
            int temp = width;
            width = height;
            height = temp;
        }
        screenResolution = new Point(width, height);
        Log.i(TAG, "Screen resolution: " + screenResolution);
        cameraResolution = findBestPreviewSizeValue(parameters, screenResolution, false);


        Log.i(TAG, "Camera resolution: " + cameraResolution);
    }

    void setDesiredCameraParameters(Camera camera) {
        Camera.Parameters parameters = camera.getParameters();
        //parameters.setAutoExposureLock(true);
        //parameters.setAutoWhiteBalanceLock(true);


        //camera.setDisplayOrientation(90);

        if (parameters == null) {
            Log.w(TAG, "Device error: no camera parameters are available. Proceeding without configuration.");
            return;
        }

//        initializeTorch(parameters);
//        String focusMode = findSettableValue(parameters.getSupportedFocusModes(), Camera.Parameters.FOCUS_MODE_AUTO, Camera.Parameters.FOCUS_MODE_MACRO) ; //, Camera.Parameters.FOCUS_MODE_MACRO);
//
//        if (focusMode != null) {
//            Log.e(TAG, "XXX " + focusMode);
//            parameters.setFocusMode(focusMode);
//        }

        //parameters.getFocus

//        List<String> focusModes = parameters.getSupportedFocusModes();
//        //String CAF_PICTURE = Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE ; //,
//String                CAF_VIDEO = Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO; //,
//                String supportedMode = CAF_VIDEO ; //focusModes
//////                        .contains(CAF_PICTURE) ? CAF_PICTURE : focusModes
//////                        .contains(CAF_VIDEO) ? CAF_VIDEO : "";
////
//        if (!supportedMode.equals("")) {
//            parameters.setFocusMode(supportedMode);
//
//        }

//        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.HONEYCOMB_MR2) {
//            parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
//            Log.d(TAG, "setting .setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE)");
//        } else {
//            parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO);
//            Log.d(TAG, "setting .setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO)");
//        }

        parameters.setPreviewSize(cameraResolution.x, cameraResolution.y);

//        Camera.Area area = new Camera.Area(new Rect(-500, -500, 500, 500), 800);//put 0 or max 1000 weight but not any changes.
//        ArrayList<Camera.Area> listArea = new ArrayList<Camera.Area>();
//        listArea.add(area);
//        parameters.setFocusAreas(listArea);
//        parameters.setAutoExposureLock(false);
//        parameters.setAutoWhiteBalanceLock(false);
//        parameters.setWhiteBalance(Camera.Parameters.WHITE_BALANCE_AUTO);
//        parameters.setAntibanding(Camera.Parameters.ANTIBANDING_AUTO);
//        parameters.setExposureCompensation(0);
//
//        parameters.setMeteringAreas(listArea);
        parameters.setSceneMode(Camera.Parameters.SCENE_MODE_AUTO);

//
        Log.e("CC", "xxx " + parameters.flatten());
        camera.setParameters(parameters);
    }

    public Point getCameraResolution() {
        return cameraResolution;
    }

    public Point getScreenResolution() {
        return screenResolution;
    }


    void setTorch(Camera camera, boolean newSetting) {
        Camera.Parameters parameters = camera.getParameters();
        doSetTorch(parameters, newSetting);
        camera.setParameters(parameters);
    }

    private static void initializeTorch(Camera.Parameters parameters) {
        //doSetTorch(parameters, true);
    }



    private static void doSetTorch(Camera.Parameters parameters, boolean newSetting) {
        String flashMode;
        if (newSetting) {
            flashMode = findSettableValue(parameters.getSupportedFlashModes(), Camera.Parameters.FLASH_MODE_TORCH, Camera.Parameters.FLASH_MODE_ON);
        } else {
            flashMode = findSettableValue(parameters.getSupportedFlashModes(), Camera.Parameters.FLASH_MODE_OFF);
        }
        if (flashMode != null) {
            parameters.setFlashMode(flashMode);
        }
    }

    private static Point findBestPreviewSizeValue(Camera.Parameters parameters, Point screenResolution, boolean portrait) {
        Point bestSize = null;
        int diff = Integer.MAX_VALUE;
        for (Camera.Size supportedPreviewSize : parameters.getSupportedPreviewSizes()) {
            int pixels = supportedPreviewSize.height * supportedPreviewSize.width;
            if (pixels < MIN_PREVIEW_PIXELS || pixels > MAX_PREVIEW_PIXELS) {
                continue;
            }
            int supportedWidth = portrait ? supportedPreviewSize.height : supportedPreviewSize.width;
            int supportedHeight = portrait ? supportedPreviewSize.width : supportedPreviewSize.height;
            int newDiff = Math.abs(screenResolution.x * supportedHeight - supportedWidth * screenResolution.y);
            if (newDiff == 0) {
                bestSize = new Point(supportedWidth, supportedHeight);
                break;
            }
            if (newDiff < diff) {
                bestSize = new Point(supportedWidth, supportedHeight);
                diff = newDiff;
            }
        }
        if (bestSize == null) {
            Camera.Size defaultSize = parameters.getPreviewSize();
            bestSize = new Point(defaultSize.width, defaultSize.height);
        }
        return bestSize;
    }

    private static String findSettableValue(Collection<String> supportedValues, String... desiredValues) {
        Log.i(TAG, "Supported values: " + supportedValues);
        String result = null;
        if (supportedValues != null) {
            for (String desiredValue : desiredValues) {
                if (supportedValues.contains(desiredValue)) {
                    result = desiredValue;
                    break;
                }
            }
        }
        Log.i(TAG, "Settable value: " + result);
        return result;
    }

}

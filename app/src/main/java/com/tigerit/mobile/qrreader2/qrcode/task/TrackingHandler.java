package com.tigerit.mobile.qrreader2.qrcode.task;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.graphics.Rect;
import android.graphics.YuvImage;
import android.hardware.Camera;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.DecodeHintType;
import com.google.zxing.LuminanceSource;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.RGBLuminanceSource;
import com.google.zxing.ReaderException;
import com.google.zxing.Result;
import com.google.zxing.ResultPoint;
import com.google.zxing.common.HybridBinarizer;
import com.tigerit.mobile.qrreader2.R;
import com.tigerit.mobile.qrreader2.qrcode.DecodeFormatManager;
import com.tigerit.mobile.qrreader2.qrcode.IDecoderListener;
import com.tigerit.mobile.qrreader2.qrcode.PlanarYUVLuminanceSource;
import com.tigerit.mobile.qrreader2.qrcode.detector.Pair;
import com.tigerit.mobile.qrreader2.qrcode.detector.QRDetect;

import com.tigerit.mobile.qrreader2.qrcode.detector.QRDetect3;
import com.tigerit.mobile.qrreader2.qrcode.detector.RegionDetect;
import com.tigerit.mobile.qrreader2.qrcode.detector.RegionDetect2;
import com.tigerit.mobile.qrreader2.qrcode.motion.data.GlobalData;
import com.tigerit.mobile.qrreader2.qrcode.motion.detection.RgbMotionDetection;
import com.tigerit.mobile.qrreader2.qrcode.motion.image.ImageProcessing;

import net.sourceforge.zbar.Config;
import net.sourceforge.zbar.Image;
import net.sourceforge.zbar.ImageScanner;
import net.sourceforge.zbar.Symbol;
import net.sourceforge.zbar.SymbolSet;

import org.opencv.android.Utils;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.imgproc.Imgproc;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;





final class TrackingHandler extends Handler {

    private static final String TAG = TrackingHandler.class.getSimpleName();

    private final IDecoderListener activity;
    private final MultiFormatReader multiFormatReader;
    private boolean running = true;

    private final QRDetect3 detector;

    private final RegionDetect rDetect;
    private final RegionDetect2 rDetect2;

    ImageScanner scanner;
    private RgbMotionDetection motionDetector;

    private Camera.AutoFocusCallback autoFocusCallback = new Camera.AutoFocusCallback() {
        @Override
        public void onAutoFocus(boolean success, Camera camera) {
            Log.e(TAG, "xxx AutoFocus Callback");
            Log.e(TAG, "xxx AutoFocus " + success);
            activity.getCameraManager().requestPreviewFrame(TrackingHandler.this, R.id.preview_view);
        }
    };

    public TrackingHandler(IDecoderListener activity) {
        multiFormatReader = new MultiFormatReader();

        Map<DecodeHintType, Object> hints = new EnumMap<DecodeHintType, Object>(DecodeHintType.class);
        hints.put(DecodeHintType.TRY_HARDER, true);
        Collection<BarcodeFormat> decodeFormats = EnumSet.noneOf(BarcodeFormat.class);
        decodeFormats.addAll(DecodeFormatManager.QR_CODE_FORMATS);
        hints.put(DecodeHintType.POSSIBLE_FORMATS, decodeFormats);
        scanner = new ImageScanner();


        scanner.setConfig(0, Config.X_DENSITY, 3);
        scanner.setConfig(0, Config.Y_DENSITY, 3);

        multiFormatReader.setHints(hints);
        this.activity = activity;
        this.detector = new QRDetect3();

        this.rDetect = new RegionDetect();
        this.rDetect2 = new RegionDetect2();
        rDetect2.activity = activity;

        this.motionDetector = new RgbMotionDetection();


    }

    @Override
    public void handleMessage(Message message) {
        if (!running) {
            return;
        }
        switch (message.what) {
            case R.id.track_polygon:
                Log.e(TAG, "xxx Track request");
                Message msg = Message.obtain(activity.getHandler(), R.id.auto_focus_clear);
                msg.sendToTarget();
                //if (!jobInProgress)
                final int W = message.arg1;
                final int H = message.arg2;
                //track((byte[])message.obj, W, H);


                try {
                    activity.getCameraManager().getCamera().cancelAutoFocus();
                    //activity.getCameraManager().setFocusArea();
                    activity.getCameraManager().getCamera().autoFocus(autoFocusCallback);
//                    activity.getCameraManager().getCamera().autoFocus(new Camera.AutoFocusCallback() {
////                        Camera.ShutterCallback shutterCallback = new Camera.ShutterCallback() {
////                            @Override
////                            public void onShutter() {
////
////                            }
////                        };
////                        Camera.PictureCallback callback = new Camera.PictureCallback() {
////                            @Override
////                            public void onPictureTaken(byte[] data, Camera camera) {
////                                    track(data, W, H);
////                            }
////                        };
//                        public void onAutoFocus(boolean success, Camera camera) {
//
//                            Log.e(TAG, "xxx auto focus");
//                            //if (success) {
//
//                                //camera.takePicture(shutterCallback, callback, null);
////                                camera.setOneShotPreviewCallback(new Camera.PreviewCallback() {
////                                    @Override
////                                    public void onPreviewFrame(byte[] data, Camera camera) {
////                                        //camera.takePicture(null, callback, null);
////                                        Log.e(TAG, "xxx Preview frame auto focus");
////                                        track(data, W, H);
////
////                                    }
////                                });
////                            } else {
////                                //camera.cancelAutoFocus();
////                                activity.getCameraManager().getCamera().autoFocus(this);
////                                Message msg = Message.obtain(activity.getHandler(), R.id.auto_focus_fail);
////                                msg.sendToTarget();
////                            }
//                        }
//                    });
                }catch (Exception ex){
                    ex.printStackTrace();
                }


                //track((byte[]) message.obj, message.arg1, message.arg2);
                break;
//            case R.id.auto_focus:
//                //decode();
//                Log.e(TAG, "Auto Focus");
//                activity.getCameraManager().requestPreviewFrame(this, R.id.decode_preview);
//                break;
            case R.id.decode_preview:
//                Log.e("DECODE", "DECODE PREVIEW");
//                decode((byte[]) message.obj, message.arg1, message.arg2);
                break;
            case R.id.preview_view:
                track((byte[])message.obj, message.arg1, message.arg2);
                break;
            case R.id.quit:
                running = false;
                Looper.myLooper().quit();
                break;
        }
    }

    private int trackFailed = 0;
    private byte[] data;
    int width, height;
    Pair<Mat, Point[]> p;
    private volatile boolean jobInProgress = false;

    /**
     * Decode the data within the viewfinder rectangle, and time how long it
     * took. For efficiency, reuse the same reader objects from one decode to
     * the next.
     *
     * @param data
     *            The YUV preview frame.
     * @param width
     *            The width of the preview frame.
     * @param height
     *            The height of the preview frame.
     */
    private void track(byte[] data, int width, int height) {

        Log.e(TAG, "track ing");
        final int W = width;
        final int H = height;




        //***PORTRAIT MODE
//        byte[] rotatedData = new byte[data.length];
//        for (int y = 0; y < height; y++) {
//            for (int x = 0; x < width; x++)
//                rotatedData[x * height + height - y - 1] = data[x + y * width];
//        }
//        data = rotatedData;
//        int temp = width;
//        width = height;
//        height = temp;



        Mat yuv = new Mat(height + height / 2 , width,CvType.CV_8UC1);
        yuv.put(0, 0, data);



        Mat frame = new Mat(height + height / 2 , width,CvType.CV_8UC1);
        Imgproc.cvtColor(yuv, frame, Imgproc.COLOR_YUV2GRAY_NV21);

        Log.e(TAG, "track : open cv conversion done");




        int rows = frame.rows();
        int rowStart = rows / 4 ;
        int rowEnd = rows * 3 / 4;
        int cols = frame.cols();
        int colStart = cols / 4;
        int colEnd = cols * 3 / 4;
        frame = frame.submat(rowStart, rowEnd, colStart, colEnd);

        //ArrayList<Pair<Mat, Point[]>> regions = rDetect.Detect(frame);

        ArrayList<Pair<Mat, Point[]>> regions = rDetect2.Detect(frame);

        if (regions != null && regions.size() > 0){
            Log.e(TAG, "Regions Detected " + regions.size());

            for (Pair<Mat, Point[]> region : regions) {
                Log.e(TAG, "track : detection done");

                Message msg = Message.obtain(activity.getHandler(), R.id.track_success);
                msg.sendToTarget();

                Mat qr_raw = region.getFirst();


                    Bitmap qrB = Bitmap.createBitmap(qr_raw.width(), qr_raw.height(), Bitmap.Config.ARGB_8888);
                    Utils.matToBitmap(qr_raw, qrB);

                    Log.e(TAG, "QR SIZE :" + qrB.getWidth() + " " + qrB.getHeight());

                    //Initialize the intArray with the same size as the number of pixels on the image
                    int[] intArray = new int[qrB.getWidth() * qrB.getHeight()];

                    //copy pixel data from the Bitmap into the 'intArray' array
                    qrB.getPixels(intArray, 0, qrB.getWidth(), 0, 0, qrB.getWidth(), qrB.getHeight());

                    RGBLuminanceSource source = new RGBLuminanceSource(qrB.getWidth(), qrB.getHeight(), intArray);

                    String qr = decodeZXing(source);


                    if (qr != null) {
                        Message ss = Message.obtain(activity.getHandler(), R.id.decode_succeeded);
                        Pair<String, Bitmap> p = new Pair<String, Bitmap>(qr, qrB);
                        ss.obj = p;
                        ss.sendToTarget();
                        Log.e(TAG, "decode success");
                        removeMessages(R.id.track_polygon);
                        removeMessages(R.id.track_failed);

                        return;
                    } else {
//                        Message ss = Message.obtain(activity.getHandler(), R.id.decode_failed);
//                        //ss.obj = regions;
//                        ss.sendToTarget();
//                        Log.e(TAG, "decode failed");

//                        Message ss1 = Message.obtain(activity.getHandler(), R.id.dump_data);
//                        ss1.obj = regions;
//                        ss.sendToTarget();
//                        Log.e(TAG, "decode failed");

                        //return;
                    }


//                } else {
//                    Message f = Message.obtain(activity.getHandler(), R.id.track_failed);
//                    f.sendToTarget();
//                    Log.e(TAG, "track failed1");
//                    return;
//                }
            }
            Message ss = Message.obtain(activity.getHandler(), R.id.decode_failed);
            ss.obj = regions;
            ss.sendToTarget();
            Log.e(TAG, "decode failed");
        }else{
            Message f = Message.obtain(activity.getHandler(), R.id.track_failed);
            f.sendToTarget();
            Log.e(TAG, "track failed2");
            return;
        }










    }

    private void sendDecodeFailure(){
        Handler handler = activity.getHandler();
        Message f = Message.obtain(activity.getHandler(), R.id.decode_failed);
        f.sendToTarget();
    }


    private void decode(byte [] data, int width, int height){

        //***PORTRAIT MODE
//        byte[] rotatedData = new byte[data.length];
//        for (int y = 0; y < height; y++) {
//            for (int x = 0; x < width; x++)
//                rotatedData[x * height + height - y - 1] = data[x + y * width];
//        }
//        data = rotatedData;
//        int temp = width;
//        width = height;
//        height = temp;


//        Mat yuv = new Mat(height + height / 2 , width,CvType.CV_8UC1);
//        yuv.put(0, 0, data);
//
//
//
//        Mat frame = new Mat(height + height / 2 , width,CvType.CV_8UC1);
//        Imgproc.cvtColor(yuv, frame, Imgproc.COLOR_YUV2GRAY_NV21);
//        Imgproc.threshold(frame, frame, 0, 25, Imgproc.THRESH_OTSU);
//
//
//        Bitmap bmp = Bitmap.createBitmap(frame.width(), frame.height(), Bitmap.Config.ARGB_8888);
//        Utils.matToBitmap(frame, bmp);

        Handler handler = activity.getHandler();


                Log.e("PREVIEW", "Motion not detected");
                String qr = decodeZXing(data, width, height);

                if (qr != null){
                    Message smsg = Message.obtain(handler, R.id.decode_succeeded);
                    Log.e("QR", "QR : " + qr);
                    smsg.obj = qr;
                    smsg.sendToTarget();
                    return;
                }else {
                    Log.e("TRACK", "QR CODE SCANNER REACHED BUT FAILED");
                    Message msg = Message.obtain(handler, R.id.decode_failed);
                    msg.sendToTarget();
                    return;
                }

    }
    private String decodeZBar(byte [] data, int width, int height){
        Image barcode = new Image(width, height, "Y800");
        barcode.setData(data);

        int result = scanner.scanImage(barcode);
        String qr = null;
        if (result != 0) {
            SymbolSet syms = scanner.getResults();
            for (Symbol sym : syms) {
                qr = sym.getData().toString();
                Log.e("QR", qr);
            }
        }
        return qr;
    }

    public String decodeZXing(byte [] data, int width, int height){
        PlanarYUVLuminanceSource source = activity.getCameraManager().buildLuminanceSource(data, width, height);
        BinaryBitmap bmp = new BinaryBitmap(new HybridBinarizer(source));

        try {
            Result result = multiFormatReader.decode(bmp);
            return result.getText();
        }catch (Exception ex){
            ex.printStackTrace();
            return null;
        }
    }

    public String decodeZXing(LuminanceSource source){

        BinaryBitmap bmp = new BinaryBitmap(new HybridBinarizer(source));

        try {
            Result result = multiFormatReader.decode(bmp);
            return result.getText();
        }catch (Exception ex){
            ex.printStackTrace();
            return null;
        }
    }

    void decodeYUV420SP(int[] rgb, byte[] yuv420sp, int width, int height) {

        final int frameSize = width * height;

        for (int j = 0, yp = 0; j < height; j++) {
            int uvp = frameSize + (j >> 1) * width, u = 0, v = 0;
            for (int i = 0; i < width; i++, yp++) {
                int y = (0xff & ((int) yuv420sp[yp])) - 16;
                if (y < 0)
                    y = 0;
                if ((i & 1) == 0) {
                    v = (0xff & yuv420sp[uvp++]) - 128;
                    u = (0xff & yuv420sp[uvp++]) - 128;
                }

                int y1192 = 1192 * y;
                int r = (y1192 + 1634 * v);
                int g = (y1192 - 833 * v - 400 * u);
                int b = (y1192 + 2066 * u);

                if (r < 0)                  r = 0;               else if (r > 262143)
                    r = 262143;
                if (g < 0)                  g = 0;               else if (g > 262143)
                    g = 262143;
                if (b < 0)                  b = 0;               else if (b > 262143)
                    b = 262143;

                rgb[yp] = 0xff000000 | ((r << 6) & 0xff0000) | ((g >> 2) & 0xff00) | ((b >> 10) & 0xff);
            }
        }
    }

}


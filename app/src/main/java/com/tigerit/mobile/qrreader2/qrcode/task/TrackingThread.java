package com.tigerit.mobile.qrreader2.qrcode.task;


import android.os.Handler;
import android.os.Looper;
import android.util.Log;


import com.google.zxing.BarcodeFormat;
import com.google.zxing.DecodeHintType;
import com.google.zxing.ResultPointCallback;
import com.tigerit.mobile.qrreader2.qrcode.IDecoderListener;

import java.util.Collection;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.Map;
import java.util.concurrent.CountDownLatch;






final class TrackingThread extends Thread {

    private static final String TAG = TrackingThread.class.getSimpleName();
    public static final String BARCODE_BITMAP = "barcode_bitmap";

    private final IDecoderListener activity;

    private final CountDownLatch handlerInitLatch;

    private Handler handler;

    public TrackingThread(IDecoderListener activity) {
        this.activity = activity;
        handlerInitLatch = new CountDownLatch(1);
    }

    public Handler getHandler() {
        try {
            handlerInitLatch.await();
        } catch (InterruptedException ie) {
            // continue?
        }
        return handler;
    }

    @Override
    public void run() {
        Log.e(TAG, "tracking thread started");
        Looper.prepare();
        handler = new TrackingHandler(activity);
        handlerInitLatch.countDown();
        Looper.loop();
    }
}


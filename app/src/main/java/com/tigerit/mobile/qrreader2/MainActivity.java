package com.tigerit.mobile.qrreader2;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;


import com.tigerit.mobile.qrreader2.fragments.CameraFragment;
import com.tigerit.mobile.qrreader2.qrcode.IQRScanner;

import org.opencv.android.OpenCVLoader;


public class MainActivity extends Activity {

    static{
        if (!OpenCVLoader.initDebug())
            Log.e("MainActivity", "OpenCV Linking failed");
        else{
            Log.e("MainActivity", "OpenCV Linking Success");
        }

        System.loadLibrary("iconv");


    }

    CameraFragment cameraFragment;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //(FrameLayout)findViewById(R.id.layout_root);
        cameraFragment = new CameraFragment();
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.add(R.id.layout_root, cameraFragment);
        ft.commit();


    }




    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to
        if (requestCode == CameraFragment.QR_DECODE) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {
                ((IQRScanner)cameraFragment).onQrScanned(data.getStringExtra("QR"), (Bitmap)data.getParcelableExtra("BitmapImage"));
            }
        }
    }
}

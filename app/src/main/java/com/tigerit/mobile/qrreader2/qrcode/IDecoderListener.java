package com.tigerit.mobile.qrreader2.qrcode;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;
import android.view.SurfaceView;

import com.google.zxing.Result;
import com.tigerit.mobile.qrreader2.qrcode.camera.CameraManager;
import com.tigerit.mobile.qrreader2.qrcode.view.ViewfinderView;


public interface IDecoderListener {
    public ViewfinderView getViewfinder();
    public Handler getHandler();
    public CameraManager getCameraManager();
    public void handleDecode(String rawResult, Bitmap barcode);
    public void handleDecode(Result rawResult, Bitmap barcode);
    public void handleDecode(String result);
    public SurfaceView getSurfaceView();
    //public void overlay(Bitmap bmp);
    //public void onScannerInitComplete();
//    public void handleFailure();
    //public void handleTrack(Bitmap bmp);
    public Context getContext();
}

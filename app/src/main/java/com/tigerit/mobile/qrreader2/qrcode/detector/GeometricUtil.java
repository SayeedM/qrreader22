package com.tigerit.mobile.qrreader2.qrcode.detector;

import java.util.List;
import java.util.Vector;

import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.imgproc.Imgproc;

public class GeometricUtil {
	
//  // Function: Routine to calculate 4 Corners of the Marker in Image Space using Region partitioning
////Theory: OpenCV Contours stores all points that describe it and these points lie the perimeter of the polygon.
////	The below function chooses the farthest points of the polygon since they form the vertices of that polygon,
////	exactly the points we are looking for. To choose the farthest point, the polygon is divided/partitioned into
////	4 regions equal regions using bounding box. Distance algorithm is applied between the centre of bounding box
////	every contour point in that region, the farthest point is deemed as the vertex of that region. Calculating
////	for all 4 regions we obtain the 4 corners of the polygon ( - quadrilateral).
  public static Vector<Point> cv_getVertices(List<MatOfPoint> contours, int c_id, double slope)
  {
	  Vector<Point> quad;
      Rect box;
      box = Imgproc.boundingRect(contours.get(c_id));

      Point M0 = new Point(),
              M1 = new Point(),
              M2 = new Point(),
              M3 = new Point();

      Point A, B = new Point(), C, D = new Point(), W = new Point(), X = new Point(),
              Y = new Point(), Z = new Point();

      A =  box.tl();
      B.x = box.br().x;
      B.y = box.tl().y;
      C = box.br();
      D.x = box.tl().x;
      D.y = box.br().y;


      W.x = (A.x + B.x) / 2;
      W.y = A.y;

      X.x = B.x;
      X.y = (B.y + C.y) / 2;

      Y.x = (C.x + D.x) / 2;
      Y.y = C.y;

      Z.x = D.x;
      Z.y = (D.y + A.y) / 2;

      double [] dmax = new double[4];
      dmax[0]=0.0;
      dmax[1]=0.0;
      dmax[2]=0.0;
      dmax[3]=0.0;

      double pd1 = 0.0;
      double pd2 = 0.0;

      if (slope > 5 || slope < -5 )
      {
          Point [] pList = contours.get(c_id).toArray();
          for( int i = 0; i < pList.length; i++ )
          {
              pd1 = cv_lineEquation(C, A, pList[i]);	// Position of point w.r.t the diagonal AC
              pd2 = cv_lineEquation(B, D, pList[i]);	// Position of point w.r.t the diagonal BD

              if((pd1 >= 0.0) && (pd2 > 0.0))
              {
                  Pair<Double, Point> p = cv_updateCorner(pList[i], W,dmax[1], M1);
                  M1 = p.getSecond();
                  dmax[1] = p.getFirst();
              }
              else if((pd1 > 0.0) && (pd2 <= 0.0))
              {
                  Pair<Double, Point> p = cv_updateCorner(pList[i], X, dmax[2], M2);
                  M2 = p.getSecond();
                  dmax[2] = p.getFirst();
              }
              else if((pd1 <= 0.0) && (pd2 < 0.0))
              {
                  Pair<Double, Point> p = cv_updateCorner(pList[i], Y, dmax[3], M3);
                  M3 = p.getSecond();
                  dmax[3] = p.getFirst();
              }
              else if((pd1 < 0.0) && (pd2 >= 0.0))
              {
                  Pair<Double, Point> p = cv_updateCorner(pList[i], Z, dmax[0], M0);
                  M0 = p.getSecond();
                  dmax[0] = p.getFirst();
              }
              else
                  continue;
          }
      }
      else
      {
          int halfx = (int)(A.x + B.x) / 2;
          int halfy = (int)(A.y + D.y) / 2;

          Point [] pList = contours.get(c_id).toArray();
          for( int i = 0; i < pList.length ; i++ )
          {
              if((pList[i].x < halfx) && (pList[i].y <= halfy))
              {
                  Pair<Double, Point> p = cv_updateCorner(pList[i],C,dmax[2],M0);
                  M0 = p.getSecond();
                  dmax[2] = p.getFirst();
              }
              else if((pList[i].x >= halfx) && (pList[i].y < halfy))
              {
                  Pair<Double, Point> p = cv_updateCorner(pList[i],D,dmax[3],M1);
                  dmax[3] = p.getFirst();
                  M1 = p.getSecond();
              }
              else if((pList[i].x > halfx) && (pList[i].y >= halfy))
              {
                  Pair<Double, Point> p = cv_updateCorner(pList[i],A,dmax[0],M2);
                  dmax[0] = p.getFirst();
                  M2 = p.getSecond();
              }
              else if((pList[i].x <= halfx) && (pList[i].y > halfy))
              {
                  Pair<Double, Point> p = cv_updateCorner(pList[i],B,dmax[1],M3);
                  dmax[1] = p.getFirst();
                  M3 = p.getSecond();
              }
          }
      }
      quad = new Vector<Point>();
      quad.add(M0);
      quad.add(M1);
      quad.add(M2);
      quad.add(M3);
      return quad;

  }
//
//  // Function: Compare a point if it more far than previously recorded farthest distance
////Description: Farthest Point detection using reference point and baseline distance
  private static Pair<Double, Point> cv_updateCorner(Point P, Point ref , double baseline,  Point corner)
  {
      double temp_dist;
      temp_dist = cv_distance(P,ref);

      if(temp_dist > baseline)
      {
          baseline = temp_dist;			// The farthest distance is the new baseline
          corner = P;						// P is now the farthest point
      }

      return new Pair<Double, Point>(baseline, corner);

  }
  
  public static double  cv_distance(Point P, Point Q)
  {
      return Math.sqrt(Math.pow(Math.abs(P.x - Q.x), 2) + Math.pow(Math.abs(P.y - Q.y), 2)) ;
  }

  
  //
	//
	////Function: Perpendicular Distance of a Point J from line formed by Points L and M; Equation of the line ax+by+c=0
	////Description: Given 3 points, the function derives the line quation of the first two points,
	////	  calculates and returns the perpendicular distance of the the 3rd point from this line.
	//
  public static double cv_lineEquation(Point L, Point M, Point J)
  {
      double a,b,c,pdist;

      a = -((M.y - L.y) / (M.x - L.x));
      b = 1.0f;
      c = (((M.y - L.y) /(M.x - L.x)) * L.x) - L.y;

      // Now that we have a, b, c from the equation ax + by + c, time to substitute (x,y) by values from the Point J

      pdist = (a * J.x + (b * J.y) + c) / Math.sqrt((a * a) + (b * b));
      return pdist;
  }
  
	  //
	////Function: Slope of a line by two Points L and M on it; Slope of line, S = (x1 -x2) / (y1- y2)
	////Description: Function returns the slope of the line formed by given 2 points, the alignement flag
	////	  indicates the line is vertical and the slope is infinity.
	//
	 public static Pair<Double, Integer> cv_lineSlope(Point L, Point M)
	 {
		 int alignment = 0;
	     double dx,dy;
	     dx = M.x - L.x;
	     dy = M.y - L.y;
	
	     if ( dy != 0)
	     {
	
	    	 alignment = 1;
	         return new Pair<Double, Integer>(dy / dx, alignment);
	     }
	     else				// Make sure we are not dividing by zero; so use 'alignement' flag
	     {
	    	 alignment = 0;
	         return new Pair<Double, Integer>(0.0, alignment);
	     }
	 }
	 
	 //
//   // Function: Sequence the Corners wrt to the orientation of the QR Code
   public static Vector<Point> cv_updateCornerOr(int orientation, Vector<Point> IN)
   {
	   Vector<Point> OUT;
       Point M0 = null ,M1 = null, M2 = null, M3 = null;
       if(orientation == 0)
       {
           M0 = IN.get(0);
           M1 = IN.get(1);
           M2 = IN.get(2);
           M3 = IN.get(3);
       }
       else if (orientation == 1)
       {
           M0 = IN.get(1);
           M1 = IN.get(2);
           M2 = IN.get(3);
           M3 = IN.get(0);
       }
       else if (orientation == 2)
       {
           M0 = IN.get(2);
           M1 = IN.get(3);
           M2 = IN.get(0);
           M3 = IN.get(1);
       }
       else if (orientation == 3)
       {
           M0 = IN.get(3);
           M1 = IN.get(0);
           M2 = IN.get(1);
           M3 = IN.get(2);
       }

       OUT = new Vector<Point>();

       OUT.add(M0);
       OUT.add(M1);
       OUT.add(M2);
       OUT.add(M3);
       return OUT;
   }
   
   //
	// // Function: Get the Intersection Point of the lines formed by sets of two points
	 public static Pair<Boolean, Point> getIntersectionPoint(Point a1, Point a2, Point b1, Point b2)
	 {
		 Point intersection;
	     Point p = a1;
	     Point q = b1;
	     Point r = new Point(a2.x -a1.x, a2.y - a1.y);
	     Point s = new Point(b2.x-b1.x, b2.y - b1.y);
	
	     if(cross(r,s) == 0) {
	         return new Pair<Boolean, Point>(false, new Point());
	     }
	
	     double t = cross(new Point(q.x -p.x, q.y - p.y),s)/cross(r,s);
	
	     Point sP = new Point(t*r.x, t*r.y);
	     intersection = new Point(p.x + sP.x, p.y + sP.y);
	     return new Pair<Boolean, Point>(true, intersection);
	 }
	
	 public static double cross(Point v1,Point v2)
	 {
	     return v1.x*v2.y - v1.y*v2.x;
	 }

  
	

}

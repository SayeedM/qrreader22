package com.tigerit.mobile.qrreader2.qrcode.detector;

import android.util.Log;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.RotatedRect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;
import org.opencv.utils.Converters;

import java.util.ArrayList;
import java.util.List;




public class RegionDetect {

    public ArrayList<Pair<Mat, Point[]>> Detect(Mat gray){

        Mat thr = new Mat(gray.size(), gray.depth());

//        Imgproc.adaptiveThreshold(gray, // Input image
//                thr,// Result binary image
//                255, //
//                Imgproc.ADAPTIVE_THRESH_GAUSSIAN_C, //
//                Imgproc.THRESH_BINARY_INV, //
//                9, //
//                9 //
//        );
//
//        Mat delement = Imgproc.getStructuringElement (Imgproc.MORPH_RECT, new Size(33, 33), new Point(1,1)  );
//        Imgproc.dilate(thr, thr, delement);
//        Imgproc.threshold(thr, thr, 0, 255, Imgproc.THRESH_OTSU);
//        Imgproc.GaussianBlur(thr, thr, new Size(15, 15), 0); //45 45
////
//        Imgproc.threshold(thr, thr, 0, 255, Imgproc.THRESH_OTSU);





        Imgproc.adaptiveThreshold(gray, thr, 255.0D, 1, 1, 9, 9);

        Imgproc.threshold(thr, thr, 00D, 255.0D, 8);
        Imgproc.dilate(thr, thr, Imgproc.getStructuringElement(0, new Size(33, 33), new Point(1.0D, 1.0D)));
        Imgproc.GaussianBlur(thr, thr, new Size(15.0D, 15.0D), 0.0D);
        Imgproc.threshold(thr, thr, 0, 255.0D, 8);





        List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
        Mat hierarchy = new Mat();
        Imgproc.findContours( thr.clone(), contours, hierarchy, Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE );






        MatOfPoint2f approx = new MatOfPoint2f();

        ArrayList<MatOfPoint> squares = new ArrayList<MatOfPoint>();
        ArrayList<Integer> squareIndex = new ArrayList<Integer>();

        double area = 0.0;
        int index = -1;

        for (int i = 0; i < contours.size(); i++)
        {
            Imgproc.approxPolyDP(new MatOfPoint2f(contours.get(i).toArray()), approx,
                    Imgproc.arcLength(new MatOfPoint2f(contours.get(i).toArray()), true)*0.02, true);



            if (approx.toArray().length == 4 )
            {
                double maxCosine = 0;

                Point[] p = approx.toArray();
                for( int j = 2; j < 5; j++ )
                {
                    double cosine = Math.abs(angle(p[j%4], p[j-2], p[j-1]));
                    maxCosine = (maxCosine < cosine ? maxCosine : cosine);
                }


                if( maxCosine < 0.3 ){
                    double tArea = Imgproc.contourArea(approx);


                    if (tArea >= 2000){
                        RotatedRect box = Imgproc.minAreaRect(new MatOfPoint2f(contours.get(i).toArray()));
                        if (box != null) {
                            Point[] corners = new Point[4];
                            box.points(corners);
                            Point p1 = corners[0];
                            Point p2 = corners[1];
                            Point p3 = corners[2];
                            double l1 = Math.sqrt((p1.x - p2.x) * (p1.x - p2.x) + (p1.y - p2.y) * (p1.y - p2.y));
                            double l2 = Math.sqrt((p2.x - p3.x) * (p2.x - p3.x) + (p2.y - p3.y) * (p2.y - p3.y));

                            if (l1 / l2 >= .75 && l1 / l2 <= 1.5) {
                                area = tArea;
                                index = i;
                            }
                        }
                        squares.add(new MatOfPoint(approx.toArray()));
                        squareIndex.add(i);
                    }

                }
            }

        }

        Point [] p = null;


        ArrayList<Pair<Mat, Point[]>> resultList = new ArrayList<Pair<Mat, Point[]>>();

        if (contours != null && contours.size() > 0 && squareIndex.size() > 0) {
            for(int i = 0 ; i < squareIndex.size() ; i++) {

                Mat qr_raw = Mat.zeros(500, 500, CvType.CV_8UC1);

                RotatedRect box = Imgproc.minAreaRect(new MatOfPoint2f(contours.get(squareIndex.get(i)).toArray()));
                Point[] corners = new Point[4];


                box.points(corners);
                Log.e("DETECTED AREA", "AREA : " + area);
                p = corners;

                ArrayList<Point> cornersList = new ArrayList<Point>();
                ArrayList<Point> transform = new ArrayList<Point>();

                cornersList.add(p[0]);
                cornersList.add(p[1]);
                cornersList.add(p[2]);
                cornersList.add(p[3]);

                Log.e("REGION", "Added Four Corners");

                /**
                 * Transforming to get the cropped image
                 */

                transform.add(new Point(20, 20));
                transform.add(new Point(450, 20));
                transform.add(new Point(450, 450));
                transform.add(new Point(20, 450));

                /**
                 * Now get the transformed qr
                 */
                Mat startM = Converters.vector_Point2f_to_Mat(cornersList);
                Mat endM = Converters.vector_Point2f_to_Mat(transform);


                Mat warp_matrix = Imgproc.getPerspectiveTransform(startM, endM);

                Imgproc.warpPerspective(gray, qr_raw, warp_matrix, new Size(500, 500));
                Imgproc.copyMakeBorder(qr_raw, qr_raw, 10, 10, 10, 10, Imgproc.BORDER_CONSTANT, new Scalar(255, 255, 255));

                Mat temp = new Mat(qr_raw.size(), qr_raw.depth());
                Imgproc.GaussianBlur(qr_raw, temp, new Size(0,0), 10);
                //Core.addWeighted(qr_raw, 2, temp, -.99, 0, qr_raw);
                Core.addWeighted(qr_raw, 2.5, temp, -.99, 0, qr_raw);






                resultList.add(new Pair<Mat, Point[]>(qr_raw, corners));
            }
        }







        Log.e("IMAGE PROC",  " region List " + resultList.size());
        return resultList;


    }



    private double angle(Point pt1, Point pt2, Point pt0)
    {
        double dx1 = pt1.x - pt0.x;
        double dy1 = pt1.y - pt0.y;
        double dx2 = pt2.x - pt0.x;
        double dy2 = pt2.y - pt0.y;
        return (dx1*dx2 + dy1*dy2)/Math.sqrt((dx1*dx1 + dy1*dy1)*(dx2*dx2 + dy2*dy2) + 1e-10);
    }

}

